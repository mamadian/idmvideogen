# README #

**PROJET VIDEOGEN**

This readme explains how to deploy your application and what technologies we use. It also explains how to test the application.

### What is in this repository ###

* The IDM directory contains :

     The  Xtext project:
      This project contains our [grammar](https://bitbucket.org/mamadian/idmvideogen/src/9d67f1be90821c33c56a89eb05943ba005297e0f/IDM/org.xtext.example.videoGen/src/org/xtext/example/videoGen/VideoGen.xtext?at=master&fileviewer=file-view-default) and all the [changes](https://bitbucket.org/mamadian/idmvideogen/src/9d67f1be90821c33c56a89eb05943ba005297e0f/IDM/org.xtext.example.videoGen/src/org/xtext/example/videoGen/generator/VideoGenGenerator.xtend?at=master&fileviewer=file-view-default) we made

     The Ecore Project: This project contains the [playlist](https://bitbucket.org/mamadian/idmvideogen/src/9d67f1be90821c33c56a89eb05943ba005297e0f/IDM/PlayListeEcore/model/playListeEcore.ecore?at=master&fileviewer=file-view-default) model written In Ecore. This will allow for after to make a model-to-model transformation

* The runtime-EclipseApplication directory :
    This folder contains a VideoGen project. This project contains a file [index.videoGen](https://bitbucket.org/mamadian/idmvideogen/src/9d67f1be90821c33c56a89eb05943ba005297e0f/runtime-EclipseApplication/VideoGen/src/index.videoGen?at=master&fileviewer=file-view-default) consistent with our grammar. it also contains a [src-gen](https://bitbucket.org/mamadian/idmvideogen/src/9d67f1be90821c33c56a89eb05943ba005297e0f/runtime-EclipseApplication/VideoGen/src-gen/?at=master) PACKAGE containing all files obtained after processing.So in this case we put all the neccessary file to start flowplayer and a style sheet to generate html page.


### How do I get set up? ###

* Download the entire project
* unzip the contents
* Open eclipse choosing as worspace the IDM folder
* Import all the projects located in
* In the file [index.videoGen](https://bitbucket.org/mamadian/idmvideogen/src/9d67f1be90821c33c56a89eb05943ba005297e0f/runtime-EclipseApplication/VideoGen/src/index.videoGen?at=master&fileviewer=file-view-default) put the proper paths to videos.
* We can generate all the neccessary files to play videos on a web page

### How to load the html page? ###
* Positioning in the src-gen folder where the files generate  are located
* Then launch an http server "http-server"

## What we has not been done ##
   The questions  on the development of the website have not all been done for lack of time.
   button "générer automatiquement" et "générer à partir de la selection" does not work
   