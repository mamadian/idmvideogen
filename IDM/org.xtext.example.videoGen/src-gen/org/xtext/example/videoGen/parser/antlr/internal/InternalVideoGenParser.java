package org.xtext.example.videoGen.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.videoGen.services.VideoGenGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalVideoGenParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'VideoGen'", "'{'", "'}'", "'mandatory'", "'Optional'", "'Alternative'", "'VideoSeq'", "','", "'Description'", "'Duree'", "'Probabilte'", "'%'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalVideoGenParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalVideoGenParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalVideoGenParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g"; }



     	private VideoGenGrammarAccess grammarAccess;
     	
        public InternalVideoGenParser(TokenStream input, VideoGenGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "VideoGen";	
       	}
       	
       	@Override
       	protected VideoGenGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleVideoGen"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:67:1: entryRuleVideoGen returns [EObject current=null] : iv_ruleVideoGen= ruleVideoGen EOF ;
    public final EObject entryRuleVideoGen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideoGen = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:68:2: (iv_ruleVideoGen= ruleVideoGen EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:69:2: iv_ruleVideoGen= ruleVideoGen EOF
            {
             newCompositeNode(grammarAccess.getVideoGenRule()); 
            pushFollow(FOLLOW_ruleVideoGen_in_entryRuleVideoGen75);
            iv_ruleVideoGen=ruleVideoGen();

            state._fsp--;

             current =iv_ruleVideoGen; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoGen85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideoGen"


    // $ANTLR start "ruleVideoGen"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:76:1: ruleVideoGen returns [EObject current=null] : ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_video_3_0= ruleVideo ) )* otherlv_4= '}' ) ;
    public final EObject ruleVideoGen() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_video_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:79:28: ( ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_video_3_0= ruleVideo ) )* otherlv_4= '}' ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:80:1: ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_video_3_0= ruleVideo ) )* otherlv_4= '}' )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:80:1: ( () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_video_3_0= ruleVideo ) )* otherlv_4= '}' )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:80:2: () otherlv_1= 'VideoGen' otherlv_2= '{' ( (lv_video_3_0= ruleVideo ) )* otherlv_4= '}'
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:80:2: ()
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getVideoGenAccess().getVideoGenAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,11,FOLLOW_11_in_ruleVideoGen131); 

                	newLeafNode(otherlv_1, grammarAccess.getVideoGenAccess().getVideoGenKeyword_1());
                
            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleVideoGen143); 

                	newLeafNode(otherlv_2, grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:94:1: ( (lv_video_3_0= ruleVideo ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=14 && LA1_0<=16)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:95:1: (lv_video_3_0= ruleVideo )
            	    {
            	    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:95:1: (lv_video_3_0= ruleVideo )
            	    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:96:3: lv_video_3_0= ruleVideo
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getVideoGenAccess().getVideoVideoParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleVideo_in_ruleVideoGen164);
            	    lv_video_3_0=ruleVideo();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getVideoGenRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"video",
            	            		lv_video_3_0, 
            	            		"Video");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleVideoGen177); 

                	newLeafNode(otherlv_4, grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideoGen"


    // $ANTLR start "entryRuleVideo"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:124:1: entryRuleVideo returns [EObject current=null] : iv_ruleVideo= ruleVideo EOF ;
    public final EObject entryRuleVideo() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideo = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:125:2: (iv_ruleVideo= ruleVideo EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:126:2: iv_ruleVideo= ruleVideo EOF
            {
             newCompositeNode(grammarAccess.getVideoRule()); 
            pushFollow(FOLLOW_ruleVideo_in_entryRuleVideo213);
            iv_ruleVideo=ruleVideo();

            state._fsp--;

             current =iv_ruleVideo; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideo223); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideo"


    // $ANTLR start "ruleVideo"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:133:1: ruleVideo returns [EObject current=null] : (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional | this_Alternative_2= ruleAlternative ) ;
    public final EObject ruleVideo() throws RecognitionException {
        EObject current = null;

        EObject this_Mandatory_0 = null;

        EObject this_Optional_1 = null;

        EObject this_Alternative_2 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:136:28: ( (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional | this_Alternative_2= ruleAlternative ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:137:1: (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional | this_Alternative_2= ruleAlternative )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:137:1: (this_Mandatory_0= ruleMandatory | this_Optional_1= ruleOptional | this_Alternative_2= ruleAlternative )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt2=1;
                }
                break;
            case 15:
                {
                alt2=2;
                }
                break;
            case 16:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:138:5: this_Mandatory_0= ruleMandatory
                    {
                     
                            newCompositeNode(grammarAccess.getVideoAccess().getMandatoryParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleMandatory_in_ruleVideo270);
                    this_Mandatory_0=ruleMandatory();

                    state._fsp--;

                     
                            current = this_Mandatory_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:148:5: this_Optional_1= ruleOptional
                    {
                     
                            newCompositeNode(grammarAccess.getVideoAccess().getOptionalParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleOptional_in_ruleVideo297);
                    this_Optional_1=ruleOptional();

                    state._fsp--;

                     
                            current = this_Optional_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:158:5: this_Alternative_2= ruleAlternative
                    {
                     
                            newCompositeNode(grammarAccess.getVideoAccess().getAlternativeParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleAlternative_in_ruleVideo324);
                    this_Alternative_2=ruleAlternative();

                    state._fsp--;

                     
                            current = this_Alternative_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideo"


    // $ANTLR start "entryRuleMandatory"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:174:1: entryRuleMandatory returns [EObject current=null] : iv_ruleMandatory= ruleMandatory EOF ;
    public final EObject entryRuleMandatory() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMandatory = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:175:2: (iv_ruleMandatory= ruleMandatory EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:176:2: iv_ruleMandatory= ruleMandatory EOF
            {
             newCompositeNode(grammarAccess.getMandatoryRule()); 
            pushFollow(FOLLOW_ruleMandatory_in_entryRuleMandatory359);
            iv_ruleMandatory=ruleMandatory();

            state._fsp--;

             current =iv_ruleMandatory; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMandatory369); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMandatory"


    // $ANTLR start "ruleMandatory"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:183:1: ruleMandatory returns [EObject current=null] : (otherlv_0= 'mandatory' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) ) ;
    public final EObject ruleMandatory() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_videoSeq_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:186:28: ( (otherlv_0= 'mandatory' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:187:1: (otherlv_0= 'mandatory' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:187:1: (otherlv_0= 'mandatory' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:187:3: otherlv_0= 'mandatory' ( (lv_videoSeq_1_0= ruleVideoSeq ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleMandatory406); 

                	newLeafNode(otherlv_0, grammarAccess.getMandatoryAccess().getMandatoryKeyword_0());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:191:1: ( (lv_videoSeq_1_0= ruleVideoSeq ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:192:1: (lv_videoSeq_1_0= ruleVideoSeq )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:192:1: (lv_videoSeq_1_0= ruleVideoSeq )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:193:3: lv_videoSeq_1_0= ruleVideoSeq
            {
             
            	        newCompositeNode(grammarAccess.getMandatoryAccess().getVideoSeqVideoSeqParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVideoSeq_in_ruleMandatory427);
            lv_videoSeq_1_0=ruleVideoSeq();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMandatoryRule());
            	        }
                   		set(
                   			current, 
                   			"videoSeq",
                    		lv_videoSeq_1_0, 
                    		"VideoSeq");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMandatory"


    // $ANTLR start "entryRuleOptional"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:217:1: entryRuleOptional returns [EObject current=null] : iv_ruleOptional= ruleOptional EOF ;
    public final EObject entryRuleOptional() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptional = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:218:2: (iv_ruleOptional= ruleOptional EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:219:2: iv_ruleOptional= ruleOptional EOF
            {
             newCompositeNode(grammarAccess.getOptionalRule()); 
            pushFollow(FOLLOW_ruleOptional_in_entryRuleOptional463);
            iv_ruleOptional=ruleOptional();

            state._fsp--;

             current =iv_ruleOptional; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOptional473); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptional"


    // $ANTLR start "ruleOptional"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:226:1: ruleOptional returns [EObject current=null] : (otherlv_0= 'Optional' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) ) ;
    public final EObject ruleOptional() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_videoSeq_1_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:229:28: ( (otherlv_0= 'Optional' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:230:1: (otherlv_0= 'Optional' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:230:1: (otherlv_0= 'Optional' ( (lv_videoSeq_1_0= ruleVideoSeq ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:230:3: otherlv_0= 'Optional' ( (lv_videoSeq_1_0= ruleVideoSeq ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleOptional510); 

                	newLeafNode(otherlv_0, grammarAccess.getOptionalAccess().getOptionalKeyword_0());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:234:1: ( (lv_videoSeq_1_0= ruleVideoSeq ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:235:1: (lv_videoSeq_1_0= ruleVideoSeq )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:235:1: (lv_videoSeq_1_0= ruleVideoSeq )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:236:3: lv_videoSeq_1_0= ruleVideoSeq
            {
             
            	        newCompositeNode(grammarAccess.getOptionalAccess().getVideoSeqVideoSeqParserRuleCall_1_0()); 
            	    
            pushFollow(FOLLOW_ruleVideoSeq_in_ruleOptional531);
            lv_videoSeq_1_0=ruleVideoSeq();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOptionalRule());
            	        }
                   		set(
                   			current, 
                   			"videoSeq",
                    		lv_videoSeq_1_0, 
                    		"VideoSeq");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptional"


    // $ANTLR start "entryRuleAlternative"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:260:1: entryRuleAlternative returns [EObject current=null] : iv_ruleAlternative= ruleAlternative EOF ;
    public final EObject entryRuleAlternative() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAlternative = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:261:2: (iv_ruleAlternative= ruleAlternative EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:262:2: iv_ruleAlternative= ruleAlternative EOF
            {
             newCompositeNode(grammarAccess.getAlternativeRule()); 
            pushFollow(FOLLOW_ruleAlternative_in_entryRuleAlternative567);
            iv_ruleAlternative=ruleAlternative();

            state._fsp--;

             current =iv_ruleAlternative; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAlternative577); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAlternative"


    // $ANTLR start "ruleAlternative"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:269:1: ruleAlternative returns [EObject current=null] : (otherlv_0= 'Alternative' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_videoSeq_3_0= ruleVideoSeq ) )+ otherlv_4= '}' ) ;
    public final EObject ruleAlternative() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_videoSeq_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:272:28: ( (otherlv_0= 'Alternative' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_videoSeq_3_0= ruleVideoSeq ) )+ otherlv_4= '}' ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:273:1: (otherlv_0= 'Alternative' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_videoSeq_3_0= ruleVideoSeq ) )+ otherlv_4= '}' )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:273:1: (otherlv_0= 'Alternative' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_videoSeq_3_0= ruleVideoSeq ) )+ otherlv_4= '}' )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:273:3: otherlv_0= 'Alternative' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_videoSeq_3_0= ruleVideoSeq ) )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,16,FOLLOW_16_in_ruleAlternative614); 

                	newLeafNode(otherlv_0, grammarAccess.getAlternativeAccess().getAlternativeKeyword_0());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:277:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:278:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:278:1: (lv_name_1_0= RULE_ID )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:279:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAlternative631); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getAlternativeAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getAlternativeRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleAlternative649); 

                	newLeafNode(otherlv_2, grammarAccess.getAlternativeAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:299:1: ( (lv_videoSeq_3_0= ruleVideoSeq ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:300:1: (lv_videoSeq_3_0= ruleVideoSeq )
            	    {
            	    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:300:1: (lv_videoSeq_3_0= ruleVideoSeq )
            	    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:301:3: lv_videoSeq_3_0= ruleVideoSeq
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAlternativeAccess().getVideoSeqVideoSeqParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleVideoSeq_in_ruleAlternative670);
            	    lv_videoSeq_3_0=ruleVideoSeq();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAlternativeRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"videoSeq",
            	            		lv_videoSeq_3_0, 
            	            		"VideoSeq");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleAlternative683); 

                	newLeafNode(otherlv_4, grammarAccess.getAlternativeAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAlternative"


    // $ANTLR start "entryRuleVideoSeq"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:329:1: entryRuleVideoSeq returns [EObject current=null] : iv_ruleVideoSeq= ruleVideoSeq EOF ;
    public final EObject entryRuleVideoSeq() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVideoSeq = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:330:2: (iv_ruleVideoSeq= ruleVideoSeq EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:331:2: iv_ruleVideoSeq= ruleVideoSeq EOF
            {
             newCompositeNode(grammarAccess.getVideoSeqRule()); 
            pushFollow(FOLLOW_ruleVideoSeq_in_entryRuleVideoSeq719);
            iv_ruleVideoSeq=ruleVideoSeq();

            state._fsp--;

             current =iv_ruleVideoSeq; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoSeq729); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVideoSeq"


    // $ANTLR start "ruleVideoSeq"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:338:1: ruleVideoSeq returns [EObject current=null] : (otherlv_0= 'VideoSeq' ( (lv_name_1_0= RULE_ID ) )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= ',' ( (lv_description_4_0= ruleDescription ) ) )? (otherlv_5= ',' ( (lv_duree_6_0= ruleDuree ) ) )? (otherlv_7= ',' ( (lv_probabilte_8_0= ruleProbabilte ) ) )? ) ;
    public final EObject ruleVideoSeq() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_text_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_description_4_0 = null;

        EObject lv_duree_6_0 = null;

        EObject lv_probabilte_8_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:341:28: ( (otherlv_0= 'VideoSeq' ( (lv_name_1_0= RULE_ID ) )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= ',' ( (lv_description_4_0= ruleDescription ) ) )? (otherlv_5= ',' ( (lv_duree_6_0= ruleDuree ) ) )? (otherlv_7= ',' ( (lv_probabilte_8_0= ruleProbabilte ) ) )? ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:342:1: (otherlv_0= 'VideoSeq' ( (lv_name_1_0= RULE_ID ) )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= ',' ( (lv_description_4_0= ruleDescription ) ) )? (otherlv_5= ',' ( (lv_duree_6_0= ruleDuree ) ) )? (otherlv_7= ',' ( (lv_probabilte_8_0= ruleProbabilte ) ) )? )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:342:1: (otherlv_0= 'VideoSeq' ( (lv_name_1_0= RULE_ID ) )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= ',' ( (lv_description_4_0= ruleDescription ) ) )? (otherlv_5= ',' ( (lv_duree_6_0= ruleDuree ) ) )? (otherlv_7= ',' ( (lv_probabilte_8_0= ruleProbabilte ) ) )? )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:342:3: otherlv_0= 'VideoSeq' ( (lv_name_1_0= RULE_ID ) )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= ',' ( (lv_description_4_0= ruleDescription ) ) )? (otherlv_5= ',' ( (lv_duree_6_0= ruleDuree ) ) )? (otherlv_7= ',' ( (lv_probabilte_8_0= ruleProbabilte ) ) )?
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleVideoSeq766); 

                	newLeafNode(otherlv_0, grammarAccess.getVideoSeqAccess().getVideoSeqKeyword_0());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:346:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:347:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:347:1: (lv_name_1_0= RULE_ID )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:348:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleVideoSeq783); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getVideoSeqAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getVideoSeqRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:364:3: ( (lv_text_2_0= RULE_STRING ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:365:1: (lv_text_2_0= RULE_STRING )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:365:1: (lv_text_2_0= RULE_STRING )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:366:3: lv_text_2_0= RULE_STRING
            {
            lv_text_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVideoSeq806); 

            			newLeafNode(lv_text_2_0, grammarAccess.getVideoSeqAccess().getTextSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVideoSeqRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_2_0, 
                    		"STRING");
            	    

            }


            }

            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:382:2: (otherlv_3= ',' ( (lv_description_4_0= ruleDescription ) ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==19) ) {
                    alt6=1;
                }
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:382:4: otherlv_3= ',' ( (lv_description_4_0= ruleDescription ) )
                    {
                    otherlv_3=(Token)match(input,18,FOLLOW_18_in_ruleVideoSeq824); 

                        	newLeafNode(otherlv_3, grammarAccess.getVideoSeqAccess().getCommaKeyword_3_0());
                        
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:386:1: ( (lv_description_4_0= ruleDescription ) )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:387:1: (lv_description_4_0= ruleDescription )
                    {
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:387:1: (lv_description_4_0= ruleDescription )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:388:3: lv_description_4_0= ruleDescription
                    {
                     
                    	        newCompositeNode(grammarAccess.getVideoSeqAccess().getDescriptionDescriptionParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleDescription_in_ruleVideoSeq845);
                    lv_description_4_0=ruleDescription();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVideoSeqRule());
                    	        }
                           		set(
                           			current, 
                           			"description",
                            		lv_description_4_0, 
                            		"Description");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:404:4: (otherlv_5= ',' ( (lv_duree_6_0= ruleDuree ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==20) ) {
                    alt7=1;
                }
            }
            switch (alt7) {
                case 1 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:404:6: otherlv_5= ',' ( (lv_duree_6_0= ruleDuree ) )
                    {
                    otherlv_5=(Token)match(input,18,FOLLOW_18_in_ruleVideoSeq860); 

                        	newLeafNode(otherlv_5, grammarAccess.getVideoSeqAccess().getCommaKeyword_4_0());
                        
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:408:1: ( (lv_duree_6_0= ruleDuree ) )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:409:1: (lv_duree_6_0= ruleDuree )
                    {
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:409:1: (lv_duree_6_0= ruleDuree )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:410:3: lv_duree_6_0= ruleDuree
                    {
                     
                    	        newCompositeNode(grammarAccess.getVideoSeqAccess().getDureeDureeParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleDuree_in_ruleVideoSeq881);
                    lv_duree_6_0=ruleDuree();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVideoSeqRule());
                    	        }
                           		set(
                           			current, 
                           			"duree",
                            		lv_duree_6_0, 
                            		"Duree");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:426:4: (otherlv_7= ',' ( (lv_probabilte_8_0= ruleProbabilte ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==18) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:426:6: otherlv_7= ',' ( (lv_probabilte_8_0= ruleProbabilte ) )
                    {
                    otherlv_7=(Token)match(input,18,FOLLOW_18_in_ruleVideoSeq896); 

                        	newLeafNode(otherlv_7, grammarAccess.getVideoSeqAccess().getCommaKeyword_5_0());
                        
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:430:1: ( (lv_probabilte_8_0= ruleProbabilte ) )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:431:1: (lv_probabilte_8_0= ruleProbabilte )
                    {
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:431:1: (lv_probabilte_8_0= ruleProbabilte )
                    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:432:3: lv_probabilte_8_0= ruleProbabilte
                    {
                     
                    	        newCompositeNode(grammarAccess.getVideoSeqAccess().getProbabilteProbabilteParserRuleCall_5_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleProbabilte_in_ruleVideoSeq917);
                    lv_probabilte_8_0=ruleProbabilte();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getVideoSeqRule());
                    	        }
                           		set(
                           			current, 
                           			"probabilte",
                            		lv_probabilte_8_0, 
                            		"Probabilte");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVideoSeq"


    // $ANTLR start "entryRuleDescription"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:456:1: entryRuleDescription returns [EObject current=null] : iv_ruleDescription= ruleDescription EOF ;
    public final EObject entryRuleDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDescription = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:457:2: (iv_ruleDescription= ruleDescription EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:458:2: iv_ruleDescription= ruleDescription EOF
            {
             newCompositeNode(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_ruleDescription_in_entryRuleDescription955);
            iv_ruleDescription=ruleDescription();

            state._fsp--;

             current =iv_ruleDescription; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDescription965); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:465:1: ruleDescription returns [EObject current=null] : (otherlv_0= 'Description' ( (lv_text_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_text_1_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:468:28: ( (otherlv_0= 'Description' ( (lv_text_1_0= RULE_STRING ) ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:469:1: (otherlv_0= 'Description' ( (lv_text_1_0= RULE_STRING ) ) )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:469:1: (otherlv_0= 'Description' ( (lv_text_1_0= RULE_STRING ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:469:3: otherlv_0= 'Description' ( (lv_text_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleDescription1002); 

                	newLeafNode(otherlv_0, grammarAccess.getDescriptionAccess().getDescriptionKeyword_0());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:473:1: ( (lv_text_1_0= RULE_STRING ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:474:1: (lv_text_1_0= RULE_STRING )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:474:1: (lv_text_1_0= RULE_STRING )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:475:3: lv_text_1_0= RULE_STRING
            {
            lv_text_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleDescription1019); 

            			newLeafNode(lv_text_1_0, grammarAccess.getDescriptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDescriptionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleDuree"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:499:1: entryRuleDuree returns [EObject current=null] : iv_ruleDuree= ruleDuree EOF ;
    public final EObject entryRuleDuree() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDuree = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:500:2: (iv_ruleDuree= ruleDuree EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:501:2: iv_ruleDuree= ruleDuree EOF
            {
             newCompositeNode(grammarAccess.getDureeRule()); 
            pushFollow(FOLLOW_ruleDuree_in_entryRuleDuree1060);
            iv_ruleDuree=ruleDuree();

            state._fsp--;

             current =iv_ruleDuree; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDuree1070); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDuree"


    // $ANTLR start "ruleDuree"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:508:1: ruleDuree returns [EObject current=null] : (otherlv_0= 'Duree' ( (lv_temps_1_0= RULE_INT ) ) ) ;
    public final EObject ruleDuree() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_temps_1_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:511:28: ( (otherlv_0= 'Duree' ( (lv_temps_1_0= RULE_INT ) ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:512:1: (otherlv_0= 'Duree' ( (lv_temps_1_0= RULE_INT ) ) )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:512:1: (otherlv_0= 'Duree' ( (lv_temps_1_0= RULE_INT ) ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:512:3: otherlv_0= 'Duree' ( (lv_temps_1_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleDuree1107); 

                	newLeafNode(otherlv_0, grammarAccess.getDureeAccess().getDureeKeyword_0());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:516:1: ( (lv_temps_1_0= RULE_INT ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:517:1: (lv_temps_1_0= RULE_INT )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:517:1: (lv_temps_1_0= RULE_INT )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:518:3: lv_temps_1_0= RULE_INT
            {
            lv_temps_1_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleDuree1124); 

            			newLeafNode(lv_temps_1_0, grammarAccess.getDureeAccess().getTempsINTTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDureeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"temps",
                    		lv_temps_1_0, 
                    		"INT");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDuree"


    // $ANTLR start "entryRuleProbabilte"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:542:1: entryRuleProbabilte returns [EObject current=null] : iv_ruleProbabilte= ruleProbabilte EOF ;
    public final EObject entryRuleProbabilte() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProbabilte = null;


        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:543:2: (iv_ruleProbabilte= ruleProbabilte EOF )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:544:2: iv_ruleProbabilte= ruleProbabilte EOF
            {
             newCompositeNode(grammarAccess.getProbabilteRule()); 
            pushFollow(FOLLOW_ruleProbabilte_in_entryRuleProbabilte1165);
            iv_ruleProbabilte=ruleProbabilte();

            state._fsp--;

             current =iv_ruleProbabilte; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProbabilte1175); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProbabilte"


    // $ANTLR start "ruleProbabilte"
    // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:551:1: ruleProbabilte returns [EObject current=null] : (otherlv_0= 'Probabilte' ( (lv_proba_1_0= RULE_INT ) ) otherlv_2= '%' ) ;
    public final EObject ruleProbabilte() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_proba_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:554:28: ( (otherlv_0= 'Probabilte' ( (lv_proba_1_0= RULE_INT ) ) otherlv_2= '%' ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:555:1: (otherlv_0= 'Probabilte' ( (lv_proba_1_0= RULE_INT ) ) otherlv_2= '%' )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:555:1: (otherlv_0= 'Probabilte' ( (lv_proba_1_0= RULE_INT ) ) otherlv_2= '%' )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:555:3: otherlv_0= 'Probabilte' ( (lv_proba_1_0= RULE_INT ) ) otherlv_2= '%'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleProbabilte1212); 

                	newLeafNode(otherlv_0, grammarAccess.getProbabilteAccess().getProbabilteKeyword_0());
                
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:559:1: ( (lv_proba_1_0= RULE_INT ) )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:560:1: (lv_proba_1_0= RULE_INT )
            {
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:560:1: (lv_proba_1_0= RULE_INT )
            // ../org.xtext.example.videoGen/src-gen/org/xtext/example/videoGen/parser/antlr/internal/InternalVideoGen.g:561:3: lv_proba_1_0= RULE_INT
            {
            lv_proba_1_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleProbabilte1229); 

            			newLeafNode(lv_proba_1_0, grammarAccess.getProbabilteAccess().getProbaINTTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getProbabilteRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"proba",
                    		lv_proba_1_0, 
                    		"INT");
            	    

            }


            }

            otherlv_2=(Token)match(input,22,FOLLOW_22_in_ruleProbabilte1246); 

                	newLeafNode(otherlv_2, grammarAccess.getProbabilteAccess().getPercentSignKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProbabilte"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleVideoGen_in_entryRuleVideoGen75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoGen85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleVideoGen131 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleVideoGen143 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_ruleVideo_in_ruleVideoGen164 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_13_in_ruleVideoGen177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideo_in_entryRuleVideo213 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideo223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_ruleVideo270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_ruleVideo297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternative_in_ruleVideo324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_entryRuleMandatory359 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMandatory369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleMandatory406 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_ruleMandatory427 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_entryRuleOptional463 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOptional473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleOptional510 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_ruleOptional531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternative_in_entryRuleAlternative567 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAlternative577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleAlternative614 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAlternative631 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleAlternative649 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_ruleAlternative670 = new BitSet(new long[]{0x0000000000022000L});
    public static final BitSet FOLLOW_13_in_ruleAlternative683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_entryRuleVideoSeq719 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoSeq729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleVideoSeq766 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleVideoSeq783 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVideoSeq806 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_ruleVideoSeq824 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_ruleDescription_in_ruleVideoSeq845 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_ruleVideoSeq860 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_ruleDuree_in_ruleVideoSeq881 = new BitSet(new long[]{0x0000000000040002L});
    public static final BitSet FOLLOW_18_in_ruleVideoSeq896 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_ruleProbabilte_in_ruleVideoSeq917 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDescription_in_entryRuleDescription955 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDescription965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleDescription1002 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleDescription1019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDuree_in_entryRuleDuree1060 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDuree1070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleDuree1107 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleDuree1124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProbabilte_in_entryRuleProbabilte1165 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProbabilte1175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleProbabilte1212 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleProbabilte1229 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleProbabilte1246 = new BitSet(new long[]{0x0000000000000002L});

}