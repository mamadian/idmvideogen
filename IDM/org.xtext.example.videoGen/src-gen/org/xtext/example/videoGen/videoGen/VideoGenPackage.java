/**
 */
package org.xtext.example.videoGen.videoGen;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.example.videoGen.videoGen.VideoGenFactory
 * @model kind="package"
 * @generated
 */
public interface VideoGenPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "videoGen";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/example/videoGen/VideoGen";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "videoGen";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  VideoGenPackage eINSTANCE = org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.VideoGenImpl <em>Video Gen</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getVideoGen()
   * @generated
   */
  int VIDEO_GEN = 0;

  /**
   * The feature id for the '<em><b>Video</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_GEN__VIDEO = 0;

  /**
   * The number of structural features of the '<em>Video Gen</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_GEN_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.VideoImpl <em>Video</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.VideoImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getVideo()
   * @generated
   */
  int VIDEO = 1;

  /**
   * The number of structural features of the '<em>Video</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.MandatoryImpl <em>Mandatory</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.MandatoryImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getMandatory()
   * @generated
   */
  int MANDATORY = 2;

  /**
   * The feature id for the '<em><b>Video Seq</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MANDATORY__VIDEO_SEQ = VIDEO_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Mandatory</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MANDATORY_FEATURE_COUNT = VIDEO_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.OptionalImpl <em>Optional</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.OptionalImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getOptional()
   * @generated
   */
  int OPTIONAL = 3;

  /**
   * The feature id for the '<em><b>Video Seq</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPTIONAL__VIDEO_SEQ = VIDEO_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Optional</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPTIONAL_FEATURE_COUNT = VIDEO_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.AlternativeImpl <em>Alternative</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.AlternativeImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getAlternative()
   * @generated
   */
  int ALTERNATIVE = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTERNATIVE__NAME = VIDEO_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Video Seq</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTERNATIVE__VIDEO_SEQ = VIDEO_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Alternative</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALTERNATIVE_FEATURE_COUNT = VIDEO_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.VideoSeqImpl <em>Video Seq</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.VideoSeqImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getVideoSeq()
   * @generated
   */
  int VIDEO_SEQ = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQ__NAME = 0;

  /**
   * The feature id for the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQ__TEXT = 1;

  /**
   * The feature id for the '<em><b>Description</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQ__DESCRIPTION = 2;

  /**
   * The feature id for the '<em><b>Duree</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQ__DUREE = 3;

  /**
   * The feature id for the '<em><b>Probabilte</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQ__PROBABILTE = 4;

  /**
   * The number of structural features of the '<em>Video Seq</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VIDEO_SEQ_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.DescriptionImpl <em>Description</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.DescriptionImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getDescription()
   * @generated
   */
  int DESCRIPTION = 6;

  /**
   * The feature id for the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION__TEXT = 0;

  /**
   * The number of structural features of the '<em>Description</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DESCRIPTION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.DureeImpl <em>Duree</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.DureeImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getDuree()
   * @generated
   */
  int DUREE = 7;

  /**
   * The feature id for the '<em><b>Temps</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DUREE__TEMPS = 0;

  /**
   * The number of structural features of the '<em>Duree</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DUREE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.videoGen.videoGen.impl.ProbabilteImpl <em>Probabilte</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.videoGen.videoGen.impl.ProbabilteImpl
   * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getProbabilte()
   * @generated
   */
  int PROBABILTE = 8;

  /**
   * The feature id for the '<em><b>Proba</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROBABILTE__PROBA = 0;

  /**
   * The number of structural features of the '<em>Probabilte</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROBABILTE_FEATURE_COUNT = 1;


  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.VideoGen <em>Video Gen</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Video Gen</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoGen
   * @generated
   */
  EClass getVideoGen();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.videoGen.videoGen.VideoGen#getVideo <em>Video</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Video</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoGen#getVideo()
   * @see #getVideoGen()
   * @generated
   */
  EReference getVideoGen_Video();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.Video <em>Video</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Video</em>'.
   * @see org.xtext.example.videoGen.videoGen.Video
   * @generated
   */
  EClass getVideo();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.Mandatory <em>Mandatory</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Mandatory</em>'.
   * @see org.xtext.example.videoGen.videoGen.Mandatory
   * @generated
   */
  EClass getMandatory();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.videoGen.videoGen.Mandatory#getVideoSeq <em>Video Seq</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Video Seq</em>'.
   * @see org.xtext.example.videoGen.videoGen.Mandatory#getVideoSeq()
   * @see #getMandatory()
   * @generated
   */
  EReference getMandatory_VideoSeq();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.Optional <em>Optional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Optional</em>'.
   * @see org.xtext.example.videoGen.videoGen.Optional
   * @generated
   */
  EClass getOptional();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.videoGen.videoGen.Optional#getVideoSeq <em>Video Seq</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Video Seq</em>'.
   * @see org.xtext.example.videoGen.videoGen.Optional#getVideoSeq()
   * @see #getOptional()
   * @generated
   */
  EReference getOptional_VideoSeq();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.Alternative <em>Alternative</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alternative</em>'.
   * @see org.xtext.example.videoGen.videoGen.Alternative
   * @generated
   */
  EClass getAlternative();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.videoGen.videoGen.Alternative#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.videoGen.videoGen.Alternative#getName()
   * @see #getAlternative()
   * @generated
   */
  EAttribute getAlternative_Name();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.videoGen.videoGen.Alternative#getVideoSeq <em>Video Seq</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Video Seq</em>'.
   * @see org.xtext.example.videoGen.videoGen.Alternative#getVideoSeq()
   * @see #getAlternative()
   * @generated
   */
  EReference getAlternative_VideoSeq();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.VideoSeq <em>Video Seq</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Video Seq</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoSeq
   * @generated
   */
  EClass getVideoSeq();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoSeq#getName()
   * @see #getVideoSeq()
   * @generated
   */
  EAttribute getVideoSeq_Name();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Text</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoSeq#getText()
   * @see #getVideoSeq()
   * @generated
   */
  EAttribute getVideoSeq_Text();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Description</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoSeq#getDescription()
   * @see #getVideoSeq()
   * @generated
   */
  EReference getVideoSeq_Description();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getDuree <em>Duree</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Duree</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoSeq#getDuree()
   * @see #getVideoSeq()
   * @generated
   */
  EReference getVideoSeq_Duree();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getProbabilte <em>Probabilte</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Probabilte</em>'.
   * @see org.xtext.example.videoGen.videoGen.VideoSeq#getProbabilte()
   * @see #getVideoSeq()
   * @generated
   */
  EReference getVideoSeq_Probabilte();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.Description <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Description</em>'.
   * @see org.xtext.example.videoGen.videoGen.Description
   * @generated
   */
  EClass getDescription();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.videoGen.videoGen.Description#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Text</em>'.
   * @see org.xtext.example.videoGen.videoGen.Description#getText()
   * @see #getDescription()
   * @generated
   */
  EAttribute getDescription_Text();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.Duree <em>Duree</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Duree</em>'.
   * @see org.xtext.example.videoGen.videoGen.Duree
   * @generated
   */
  EClass getDuree();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.videoGen.videoGen.Duree#getTemps <em>Temps</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Temps</em>'.
   * @see org.xtext.example.videoGen.videoGen.Duree#getTemps()
   * @see #getDuree()
   * @generated
   */
  EAttribute getDuree_Temps();

  /**
   * Returns the meta object for class '{@link org.xtext.example.videoGen.videoGen.Probabilte <em>Probabilte</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Probabilte</em>'.
   * @see org.xtext.example.videoGen.videoGen.Probabilte
   * @generated
   */
  EClass getProbabilte();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.videoGen.videoGen.Probabilte#getProba <em>Proba</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Proba</em>'.
   * @see org.xtext.example.videoGen.videoGen.Probabilte#getProba()
   * @see #getProbabilte()
   * @generated
   */
  EAttribute getProbabilte_Proba();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  VideoGenFactory getVideoGenFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.VideoGenImpl <em>Video Gen</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getVideoGen()
     * @generated
     */
    EClass VIDEO_GEN = eINSTANCE.getVideoGen();

    /**
     * The meta object literal for the '<em><b>Video</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VIDEO_GEN__VIDEO = eINSTANCE.getVideoGen_Video();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.VideoImpl <em>Video</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.VideoImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getVideo()
     * @generated
     */
    EClass VIDEO = eINSTANCE.getVideo();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.MandatoryImpl <em>Mandatory</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.MandatoryImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getMandatory()
     * @generated
     */
    EClass MANDATORY = eINSTANCE.getMandatory();

    /**
     * The meta object literal for the '<em><b>Video Seq</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MANDATORY__VIDEO_SEQ = eINSTANCE.getMandatory_VideoSeq();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.OptionalImpl <em>Optional</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.OptionalImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getOptional()
     * @generated
     */
    EClass OPTIONAL = eINSTANCE.getOptional();

    /**
     * The meta object literal for the '<em><b>Video Seq</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OPTIONAL__VIDEO_SEQ = eINSTANCE.getOptional_VideoSeq();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.AlternativeImpl <em>Alternative</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.AlternativeImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getAlternative()
     * @generated
     */
    EClass ALTERNATIVE = eINSTANCE.getAlternative();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ALTERNATIVE__NAME = eINSTANCE.getAlternative_Name();

    /**
     * The meta object literal for the '<em><b>Video Seq</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ALTERNATIVE__VIDEO_SEQ = eINSTANCE.getAlternative_VideoSeq();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.VideoSeqImpl <em>Video Seq</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.VideoSeqImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getVideoSeq()
     * @generated
     */
    EClass VIDEO_SEQ = eINSTANCE.getVideoSeq();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VIDEO_SEQ__NAME = eINSTANCE.getVideoSeq_Name();

    /**
     * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VIDEO_SEQ__TEXT = eINSTANCE.getVideoSeq_Text();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VIDEO_SEQ__DESCRIPTION = eINSTANCE.getVideoSeq_Description();

    /**
     * The meta object literal for the '<em><b>Duree</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VIDEO_SEQ__DUREE = eINSTANCE.getVideoSeq_Duree();

    /**
     * The meta object literal for the '<em><b>Probabilte</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VIDEO_SEQ__PROBABILTE = eINSTANCE.getVideoSeq_Probabilte();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.DescriptionImpl <em>Description</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.DescriptionImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getDescription()
     * @generated
     */
    EClass DESCRIPTION = eINSTANCE.getDescription();

    /**
     * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DESCRIPTION__TEXT = eINSTANCE.getDescription_Text();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.DureeImpl <em>Duree</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.DureeImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getDuree()
     * @generated
     */
    EClass DUREE = eINSTANCE.getDuree();

    /**
     * The meta object literal for the '<em><b>Temps</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DUREE__TEMPS = eINSTANCE.getDuree_Temps();

    /**
     * The meta object literal for the '{@link org.xtext.example.videoGen.videoGen.impl.ProbabilteImpl <em>Probabilte</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.videoGen.videoGen.impl.ProbabilteImpl
     * @see org.xtext.example.videoGen.videoGen.impl.VideoGenPackageImpl#getProbabilte()
     * @generated
     */
    EClass PROBABILTE = eINSTANCE.getProbabilte();

    /**
     * The meta object literal for the '<em><b>Proba</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROBABILTE__PROBA = eINSTANCE.getProbabilte_Proba();

  }

} //VideoGenPackage
