/**
 */
package org.xtext.example.videoGen.videoGen;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Video Seq</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.videoGen.videoGen.VideoSeq#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.videoGen.videoGen.VideoSeq#getText <em>Text</em>}</li>
 *   <li>{@link org.xtext.example.videoGen.videoGen.VideoSeq#getDescription <em>Description</em>}</li>
 *   <li>{@link org.xtext.example.videoGen.videoGen.VideoSeq#getDuree <em>Duree</em>}</li>
 *   <li>{@link org.xtext.example.videoGen.videoGen.VideoSeq#getProbabilte <em>Probabilte</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getVideoSeq()
 * @model
 * @generated
 */
public interface VideoSeq extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getVideoSeq_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Text</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Text</em>' attribute.
   * @see #setText(String)
   * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getVideoSeq_Text()
   * @model
   * @generated
   */
  String getText();

  /**
   * Sets the value of the '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getText <em>Text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Text</em>' attribute.
   * @see #getText()
   * @generated
   */
  void setText(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' containment reference.
   * @see #setDescription(Description)
   * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getVideoSeq_Description()
   * @model containment="true"
   * @generated
   */
  Description getDescription();

  /**
   * Sets the value of the '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getDescription <em>Description</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' containment reference.
   * @see #getDescription()
   * @generated
   */
  void setDescription(Description value);

  /**
   * Returns the value of the '<em><b>Duree</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Duree</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Duree</em>' containment reference.
   * @see #setDuree(Duree)
   * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getVideoSeq_Duree()
   * @model containment="true"
   * @generated
   */
  Duree getDuree();

  /**
   * Sets the value of the '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getDuree <em>Duree</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Duree</em>' containment reference.
   * @see #getDuree()
   * @generated
   */
  void setDuree(Duree value);

  /**
   * Returns the value of the '<em><b>Probabilte</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Probabilte</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Probabilte</em>' containment reference.
   * @see #setProbabilte(Probabilte)
   * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getVideoSeq_Probabilte()
   * @model containment="true"
   * @generated
   */
  Probabilte getProbabilte();

  /**
   * Sets the value of the '{@link org.xtext.example.videoGen.videoGen.VideoSeq#getProbabilte <em>Probabilte</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Probabilte</em>' containment reference.
   * @see #getProbabilte()
   * @generated
   */
  void setProbabilte(Probabilte value);

} // VideoSeq
