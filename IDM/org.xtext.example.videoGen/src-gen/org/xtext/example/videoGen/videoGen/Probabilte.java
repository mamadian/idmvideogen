/**
 */
package org.xtext.example.videoGen.videoGen;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Probabilte</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.videoGen.videoGen.Probabilte#getProba <em>Proba</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getProbabilte()
 * @model
 * @generated
 */
public interface Probabilte extends EObject
{
  /**
   * Returns the value of the '<em><b>Proba</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Proba</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Proba</em>' attribute.
   * @see #setProba(int)
   * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getProbabilte_Proba()
   * @model
   * @generated
   */
  int getProba();

  /**
   * Sets the value of the '{@link org.xtext.example.videoGen.videoGen.Probabilte#getProba <em>Proba</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Proba</em>' attribute.
   * @see #getProba()
   * @generated
   */
  void setProba(int value);

} // Probabilte
