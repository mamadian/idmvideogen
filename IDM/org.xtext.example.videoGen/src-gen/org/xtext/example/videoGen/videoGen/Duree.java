/**
 */
package org.xtext.example.videoGen.videoGen;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Duree</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.videoGen.videoGen.Duree#getTemps <em>Temps</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getDuree()
 * @model
 * @generated
 */
public interface Duree extends EObject
{
  /**
   * Returns the value of the '<em><b>Temps</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Temps</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Temps</em>' attribute.
   * @see #setTemps(int)
   * @see org.xtext.example.videoGen.videoGen.VideoGenPackage#getDuree_Temps()
   * @model
   * @generated
   */
  int getTemps();

  /**
   * Sets the value of the '{@link org.xtext.example.videoGen.videoGen.Duree#getTemps <em>Temps</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Temps</em>' attribute.
   * @see #getTemps()
   * @generated
   */
  void setTemps(int value);

} // Duree
