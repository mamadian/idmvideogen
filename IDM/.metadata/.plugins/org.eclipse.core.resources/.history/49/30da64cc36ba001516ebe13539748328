/*
 * generated by Xtext
 */
package org.xtext.example.videoGen.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.IFileSystemAccess
import org.xtext.example.videoGen.videoGen.VideoGen
import org.xtext.example.videoGen.videoGen.Optional
import org.xtext.example.videoGen.videoGen.Mandatory
import java.util.Random
import org.xtext.example.videoGen.videoGen.Alternative
import playListeEcore.PlayListeEcoreFactory
import playListeEcore.impl.PlayListeEcoreFactoryImpl
import playListeEcore.PlayList
import org.eclipse.emf.common.command.Command
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class VideoGenGenerator implements IGenerator {
	
	override void doGenerate(Resource resource, IFileSystemAccess fsa) {
//		fsa.generateFile('greetings.txt', 'People to greet: ' + 
//			resource.allContents
//				.filter(typeof(Greeting))
//				.map[name]
//				.join(', '))
		var uri = resource.URI.lastSegment;
		for(VideoGen: resource.allContents.toIterable.filter(typeof(VideoGen))){
			var playList = this.VideoGenToPlayList(VideoGen)
			fsa.generateFile(uri.substring(0, uri.indexOf('.')) + '.txt' ,playList.compile_ffmpeg)
			fsa.generateFile(uri.substring(0, uri.indexOf('.')) + '.m3u' ,playList.compile_M3U)
			fsa.generateFile(uri.substring(0, uri.indexOf('.')) + '.m2ue' ,VideoGen.videoGenToM3UE)
			fsa.generateFile(uri.substring(0, uri.indexOf('.')) + '.txt' ,VideoGen.compile)
			fsa.generateFile(uri.substring(0, uri.indexOf('.')) + '.html' ,VideoGen.VideoGenToPageWeb)
		}
		
		
	}
	
	/**
	 * 
	 */
	def isOptional(){
		var rd=  new Random
		var i = rd.nextInt(1);
		return (i==1);
	}
	
	/**
	 * 
	 */
	def recupAlternative ( Alternative alt){
		var rd= new Random
		var i=  rd.nextInt(alt.videoSeq.size)
		var tab_alter = alt.videoSeq
		return tab_alter.get(i)
	}
	
	/**
	 * 
	 */
	def compile (VideoGen videoGen){
		'''
	«FOR p : videoGen.video»
		«IF p instanceof Mandatory»
			file '«p.videoSeq.text»'
		«ENDIF»
		«IF p instanceof Optional»
			«IF this.isOptional()»
				file '«p.videoSeq.text»'
			«ENDIF»
		«ENDIF»
		«IF p instanceof Alternative»
			«var vAlt = this.recupAlternative(p)»
			file '«vAlt.text»'
		«ENDIF»
	«ENDFOR»
		'''
	}
	
	
	/**
	 * 
	 */
	def VideoGenToPlayList(VideoGen videoGen){
		var playListFactory = new PlayListeEcoreFactoryImpl
		var playList = playListFactory.createPlayList
		for(p: videoGen.video){
				if( p instanceof Mandatory){
					var video = playListFactory.createVideo
					video.lien  = p.videoSeq.text
					playList.video.add(video)
				}
				else if(p instanceof Optional){
					if(this.isOptional()){
						var video = playListFactory.createVideo
						video.lien = p.videoSeq.text
						playList.video.add(video)
					}
				}
				else if(p instanceof Alternative){
					var video = playListFactory.createVideo
					var vAlt=this.recupAlternative(p)
					video.lien = vAlt.text
					playList.video.add(video)
				}
			 
		}
		return playList
	}
	
	
	/**
	 * 
	 */
	def compile_ffmpeg (PlayList playlist){
		'''
		#Commentaire
		« FOR video : playlist.video»
			file '«video.lien»'
		«ENDFOR»
		
		'''
	}
	
	/**
	 * 
	 */
	def compile_M3U (PlayList playlist){
		'''
		« FOR video : playlist.video»
			«video.lien»
		«ENDFOR»
		'''
	}
	
	/**
	 * 
	 */
	def compile_M3UE (PlayList playlist){
		'''
		#EXTM3U
		« FOR video : playlist.video»
		#EXTM3U-X-
		#EXTM3U:« »
			«video.lien»
		«ENDFOR»
		#EXTM3U-ENDLIST
		'''
	}
	
	/**
	 * 
	 */
	def execute( String commande){
		var  String[] finalCommande = #["/bin/sh", "-c" ,commande]
		var process=Runtime.getRuntime().exec(finalCommande)
		var buffer= new BufferedReader(new InputStreamReader(process.getInputStream()));
		var resultat = new StringBuffer
		while (buffer.ready) {
		    resultat.append(buffer.readLine + "\n");
		}
		return resultat
	}
	
	/**
	 * 
	 */
	def videoGenToM3UE (VideoGen videoGen){
		'''
	#EXTM3U
	«FOR p : videoGen.video»
		«IF p instanceof Mandatory»
			«var command= "ffmpeg -i "+p.videoSeq.text+"2>&1 | grep 'Duration'"»
			#EXT-X-DISCONTINUITY
			#EXTINF:«execute(command)»
			«p.videoSeq.text»
		«ENDIF»
		«IF p instanceof Optional»
			«IF this.isOptional()»
				«var command= "ffmpeg -i "+p.videoSeq.text+"2>&1 | grep 'Duration'"»
				#EXT-X-DISCONTINUITY
				#EXTINF:«execute(command)»
				«p.videoSeq.text»
			«ENDIF»
		«ENDIF»
		«IF p instanceof Alternative»
			«var vAlt = this.recupAlternative(p)»
			«var command= "ffmpeg -i "+vAlt.text+"2>&1 | grep 'Duration'"»
			#EXT-X-DISCONTINUITY
			#EXTINF:«execute(command)»
			file '«vAlt.text»'
		«ENDIF»
	«ENDFOR»
	#EXT-X-ENDLIST
		'''
	}
	
	/**
	 * 
	 */
	def videoGenMp4Tots(VideoGen videoGen){
		for(p: videoGen.video){
				if( p instanceof Mandatory){
					var input = p.videoSeq.text
					var output= input.substring(0,input.indexOf('.'))+".ts"
					var commande = "ffmpeg -i "+input+" -strict -2 -vcodec h264 -acodec aac -f mpegts"+output
					execute(commande)
				}
				else if(p instanceof Optional){
					var input = p.videoSeq.text
					var output= input.substring(0,input.indexOf('.'))+".ts"
					var commande = "ffmpeg -i "+input+" -strict -2 -vcodec h264 -acodec aac -f mpegts"+output
					execute(commande)
				}
				else if(p instanceof Alternative){
					for(video:p.videoSeq){
						var input = video.text
						var output= input.substring(0,input.indexOf('.'))+".ts"
						var commande = "ffmpeg -i "+input+" -strict -2 -vcodec h264 -acodec aac -f mpegts "+output
						execute(commande)
					}
				}
		}
	}
	
	/**
	 * 
	 */
	def videoGenToVignette (VideoGen videoGen){
		for(p: videoGen.video){
				if( p instanceof Mandatory){
					var input = p.videoSeq.text
					var output= input.substring(0,input.indexOf('.'))+".png"
					var commande = "ffmpeg -i "+input+" -r 1 -t 00:00:01 -ss 00:00:03 -f image2 "+output
					execute(commande)
				}
				else if(p instanceof Optional){
					var input = p.videoSeq.text
					var output= input.substring(0,input.indexOf('.'))+".png"
					var commande = "ffmpeg -i "+input+" -r 1 -t 00:00:01 -ss 00:00:03 -f image2 "+output
					execute(commande)
				}
				else if(p instanceof Alternative){
					for(video:p.videoSeq){
						var input = video.text
						var output= input.substring(0,input.indexOf('.'))+".png"
						var commande = "ffmpeg -i "+input+" -r 1 -t 00:00:01 -ss 00:00:03 -f image2 "+output
						execute(commande)
					}
				}
		}
	}
	
	/**
	 * 
	 */
	def VideoToVignette(String video){
		var output= video.substring(0,video.indexOf('.'))+".png"
		var commande = "ffmpeg -i "+video+" -r 1 -t 00:00:01 -ss 00:00:05 -f image2 "+output
		execute(commande)
		return output
	}
	
	/**
	 * 
	 */
	 def VideoGenToPageWeb(VideoGen videoGen){
	 	'''
		<html>
		<head>
		<title>Generator de video</title>
		</head>
		<body>
		<table>
		<tr class="mandatory">
					«FOR p : videoGen.video»
						«IF p instanceof Mandatory»
							<td> <img width="100" height="70" src="«p.videoSeq.text.VideoToVignette»"/></td>
						«ENDIF»
					«ENDFOR»
					</tr>
					<tr class="optional">
					«FOR p : videoGen.video»
						«IF p instanceof Optional»
							<td><img width="100" height="70" src="«p.videoSeq.text.VideoToVignette»"/></td>
						«ENDIF»
					«ENDFOR»
					</tr>
					<tr class="alternative">
					«FOR p : videoGen.video»
						«IF p instanceof Alternative»
							«FOR video :p.videoSeq»
								<td> <img width="100" height="70" src="«video.text.VideoToVignette»"/></td>
							«ENDFOR»
						«ENDIF»
					«ENDFOR»
					</tr>
				</table>
			</body>
		</html>
		'''
	 }
}
