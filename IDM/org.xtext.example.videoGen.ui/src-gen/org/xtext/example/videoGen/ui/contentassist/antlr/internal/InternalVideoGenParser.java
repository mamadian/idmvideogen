package org.xtext.example.videoGen.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.videoGen.services.VideoGenGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalVideoGenParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'VideoGen'", "'{'", "'}'", "'mandatory'", "'Optional'", "'Alternative'", "'VideoSeq'", "','", "'Description'", "'Duree'", "'Probabilte'", "'%'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalVideoGenParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalVideoGenParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalVideoGenParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g"; }


     
     	private VideoGenGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(VideoGenGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleVideoGen"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:60:1: entryRuleVideoGen : ruleVideoGen EOF ;
    public final void entryRuleVideoGen() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:61:1: ( ruleVideoGen EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:62:1: ruleVideoGen EOF
            {
             before(grammarAccess.getVideoGenRule()); 
            pushFollow(FOLLOW_ruleVideoGen_in_entryRuleVideoGen61);
            ruleVideoGen();

            state._fsp--;

             after(grammarAccess.getVideoGenRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoGen68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVideoGen"


    // $ANTLR start "ruleVideoGen"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:69:1: ruleVideoGen : ( ( rule__VideoGen__Group__0 ) ) ;
    public final void ruleVideoGen() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:73:2: ( ( ( rule__VideoGen__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:74:1: ( ( rule__VideoGen__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:74:1: ( ( rule__VideoGen__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:75:1: ( rule__VideoGen__Group__0 )
            {
             before(grammarAccess.getVideoGenAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:76:1: ( rule__VideoGen__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:76:2: rule__VideoGen__Group__0
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__0_in_ruleVideoGen94);
            rule__VideoGen__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVideoGenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVideoGen"


    // $ANTLR start "entryRuleVideo"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:88:1: entryRuleVideo : ruleVideo EOF ;
    public final void entryRuleVideo() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:89:1: ( ruleVideo EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:90:1: ruleVideo EOF
            {
             before(grammarAccess.getVideoRule()); 
            pushFollow(FOLLOW_ruleVideo_in_entryRuleVideo121);
            ruleVideo();

            state._fsp--;

             after(grammarAccess.getVideoRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideo128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVideo"


    // $ANTLR start "ruleVideo"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:97:1: ruleVideo : ( ( rule__Video__Alternatives ) ) ;
    public final void ruleVideo() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:101:2: ( ( ( rule__Video__Alternatives ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:102:1: ( ( rule__Video__Alternatives ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:102:1: ( ( rule__Video__Alternatives ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:103:1: ( rule__Video__Alternatives )
            {
             before(grammarAccess.getVideoAccess().getAlternatives()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:104:1: ( rule__Video__Alternatives )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:104:2: rule__Video__Alternatives
            {
            pushFollow(FOLLOW_rule__Video__Alternatives_in_ruleVideo154);
            rule__Video__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getVideoAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVideo"


    // $ANTLR start "entryRuleMandatory"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:116:1: entryRuleMandatory : ruleMandatory EOF ;
    public final void entryRuleMandatory() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:117:1: ( ruleMandatory EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:118:1: ruleMandatory EOF
            {
             before(grammarAccess.getMandatoryRule()); 
            pushFollow(FOLLOW_ruleMandatory_in_entryRuleMandatory181);
            ruleMandatory();

            state._fsp--;

             after(grammarAccess.getMandatoryRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleMandatory188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMandatory"


    // $ANTLR start "ruleMandatory"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:125:1: ruleMandatory : ( ( rule__Mandatory__Group__0 ) ) ;
    public final void ruleMandatory() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:129:2: ( ( ( rule__Mandatory__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:130:1: ( ( rule__Mandatory__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:130:1: ( ( rule__Mandatory__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:131:1: ( rule__Mandatory__Group__0 )
            {
             before(grammarAccess.getMandatoryAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:132:1: ( rule__Mandatory__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:132:2: rule__Mandatory__Group__0
            {
            pushFollow(FOLLOW_rule__Mandatory__Group__0_in_ruleMandatory214);
            rule__Mandatory__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMandatoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMandatory"


    // $ANTLR start "entryRuleOptional"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:144:1: entryRuleOptional : ruleOptional EOF ;
    public final void entryRuleOptional() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:145:1: ( ruleOptional EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:146:1: ruleOptional EOF
            {
             before(grammarAccess.getOptionalRule()); 
            pushFollow(FOLLOW_ruleOptional_in_entryRuleOptional241);
            ruleOptional();

            state._fsp--;

             after(grammarAccess.getOptionalRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOptional248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptional"


    // $ANTLR start "ruleOptional"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:153:1: ruleOptional : ( ( rule__Optional__Group__0 ) ) ;
    public final void ruleOptional() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:157:2: ( ( ( rule__Optional__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:158:1: ( ( rule__Optional__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:158:1: ( ( rule__Optional__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:159:1: ( rule__Optional__Group__0 )
            {
             before(grammarAccess.getOptionalAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:160:1: ( rule__Optional__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:160:2: rule__Optional__Group__0
            {
            pushFollow(FOLLOW_rule__Optional__Group__0_in_ruleOptional274);
            rule__Optional__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptionalAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptional"


    // $ANTLR start "entryRuleAlternative"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:172:1: entryRuleAlternative : ruleAlternative EOF ;
    public final void entryRuleAlternative() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:173:1: ( ruleAlternative EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:174:1: ruleAlternative EOF
            {
             before(grammarAccess.getAlternativeRule()); 
            pushFollow(FOLLOW_ruleAlternative_in_entryRuleAlternative301);
            ruleAlternative();

            state._fsp--;

             after(grammarAccess.getAlternativeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAlternative308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAlternative"


    // $ANTLR start "ruleAlternative"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:181:1: ruleAlternative : ( ( rule__Alternative__Group__0 ) ) ;
    public final void ruleAlternative() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:185:2: ( ( ( rule__Alternative__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:186:1: ( ( rule__Alternative__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:186:1: ( ( rule__Alternative__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:187:1: ( rule__Alternative__Group__0 )
            {
             before(grammarAccess.getAlternativeAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:188:1: ( rule__Alternative__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:188:2: rule__Alternative__Group__0
            {
            pushFollow(FOLLOW_rule__Alternative__Group__0_in_ruleAlternative334);
            rule__Alternative__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAlternativeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAlternative"


    // $ANTLR start "entryRuleVideoSeq"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:200:1: entryRuleVideoSeq : ruleVideoSeq EOF ;
    public final void entryRuleVideoSeq() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:201:1: ( ruleVideoSeq EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:202:1: ruleVideoSeq EOF
            {
             before(grammarAccess.getVideoSeqRule()); 
            pushFollow(FOLLOW_ruleVideoSeq_in_entryRuleVideoSeq361);
            ruleVideoSeq();

            state._fsp--;

             after(grammarAccess.getVideoSeqRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVideoSeq368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVideoSeq"


    // $ANTLR start "ruleVideoSeq"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:209:1: ruleVideoSeq : ( ( rule__VideoSeq__Group__0 ) ) ;
    public final void ruleVideoSeq() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:213:2: ( ( ( rule__VideoSeq__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:214:1: ( ( rule__VideoSeq__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:214:1: ( ( rule__VideoSeq__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:215:1: ( rule__VideoSeq__Group__0 )
            {
             before(grammarAccess.getVideoSeqAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:216:1: ( rule__VideoSeq__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:216:2: rule__VideoSeq__Group__0
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group__0_in_ruleVideoSeq394);
            rule__VideoSeq__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVideoSeqAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVideoSeq"


    // $ANTLR start "entryRuleDescription"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:228:1: entryRuleDescription : ruleDescription EOF ;
    public final void entryRuleDescription() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:229:1: ( ruleDescription EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:230:1: ruleDescription EOF
            {
             before(grammarAccess.getDescriptionRule()); 
            pushFollow(FOLLOW_ruleDescription_in_entryRuleDescription421);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getDescriptionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDescription428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDescription"


    // $ANTLR start "ruleDescription"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:237:1: ruleDescription : ( ( rule__Description__Group__0 ) ) ;
    public final void ruleDescription() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:241:2: ( ( ( rule__Description__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:242:1: ( ( rule__Description__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:242:1: ( ( rule__Description__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:243:1: ( rule__Description__Group__0 )
            {
             before(grammarAccess.getDescriptionAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:244:1: ( rule__Description__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:244:2: rule__Description__Group__0
            {
            pushFollow(FOLLOW_rule__Description__Group__0_in_ruleDescription454);
            rule__Description__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDescription"


    // $ANTLR start "entryRuleDuree"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:256:1: entryRuleDuree : ruleDuree EOF ;
    public final void entryRuleDuree() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:257:1: ( ruleDuree EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:258:1: ruleDuree EOF
            {
             before(grammarAccess.getDureeRule()); 
            pushFollow(FOLLOW_ruleDuree_in_entryRuleDuree481);
            ruleDuree();

            state._fsp--;

             after(grammarAccess.getDureeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDuree488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDuree"


    // $ANTLR start "ruleDuree"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:265:1: ruleDuree : ( ( rule__Duree__Group__0 ) ) ;
    public final void ruleDuree() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:269:2: ( ( ( rule__Duree__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:270:1: ( ( rule__Duree__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:270:1: ( ( rule__Duree__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:271:1: ( rule__Duree__Group__0 )
            {
             before(grammarAccess.getDureeAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:272:1: ( rule__Duree__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:272:2: rule__Duree__Group__0
            {
            pushFollow(FOLLOW_rule__Duree__Group__0_in_ruleDuree514);
            rule__Duree__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDureeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDuree"


    // $ANTLR start "entryRuleProbabilte"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:284:1: entryRuleProbabilte : ruleProbabilte EOF ;
    public final void entryRuleProbabilte() throws RecognitionException {
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:285:1: ( ruleProbabilte EOF )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:286:1: ruleProbabilte EOF
            {
             before(grammarAccess.getProbabilteRule()); 
            pushFollow(FOLLOW_ruleProbabilte_in_entryRuleProbabilte541);
            ruleProbabilte();

            state._fsp--;

             after(grammarAccess.getProbabilteRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleProbabilte548); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProbabilte"


    // $ANTLR start "ruleProbabilte"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:293:1: ruleProbabilte : ( ( rule__Probabilte__Group__0 ) ) ;
    public final void ruleProbabilte() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:297:2: ( ( ( rule__Probabilte__Group__0 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:298:1: ( ( rule__Probabilte__Group__0 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:298:1: ( ( rule__Probabilte__Group__0 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:299:1: ( rule__Probabilte__Group__0 )
            {
             before(grammarAccess.getProbabilteAccess().getGroup()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:300:1: ( rule__Probabilte__Group__0 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:300:2: rule__Probabilte__Group__0
            {
            pushFollow(FOLLOW_rule__Probabilte__Group__0_in_ruleProbabilte574);
            rule__Probabilte__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProbabilteAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProbabilte"


    // $ANTLR start "rule__Video__Alternatives"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:312:1: rule__Video__Alternatives : ( ( ruleMandatory ) | ( ruleOptional ) | ( ruleAlternative ) );
    public final void rule__Video__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:316:1: ( ( ruleMandatory ) | ( ruleOptional ) | ( ruleAlternative ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt1=1;
                }
                break;
            case 15:
                {
                alt1=2;
                }
                break;
            case 16:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:317:1: ( ruleMandatory )
                    {
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:317:1: ( ruleMandatory )
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:318:1: ruleMandatory
                    {
                     before(grammarAccess.getVideoAccess().getMandatoryParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleMandatory_in_rule__Video__Alternatives610);
                    ruleMandatory();

                    state._fsp--;

                     after(grammarAccess.getVideoAccess().getMandatoryParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:323:6: ( ruleOptional )
                    {
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:323:6: ( ruleOptional )
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:324:1: ruleOptional
                    {
                     before(grammarAccess.getVideoAccess().getOptionalParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleOptional_in_rule__Video__Alternatives627);
                    ruleOptional();

                    state._fsp--;

                     after(grammarAccess.getVideoAccess().getOptionalParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:329:6: ( ruleAlternative )
                    {
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:329:6: ( ruleAlternative )
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:330:1: ruleAlternative
                    {
                     before(grammarAccess.getVideoAccess().getAlternativeParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleAlternative_in_rule__Video__Alternatives644);
                    ruleAlternative();

                    state._fsp--;

                     after(grammarAccess.getVideoAccess().getAlternativeParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Video__Alternatives"


    // $ANTLR start "rule__VideoGen__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:342:1: rule__VideoGen__Group__0 : rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1 ;
    public final void rule__VideoGen__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:346:1: ( rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:347:2: rule__VideoGen__Group__0__Impl rule__VideoGen__Group__1
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__0__Impl_in_rule__VideoGen__Group__0674);
            rule__VideoGen__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__1_in_rule__VideoGen__Group__0677);
            rule__VideoGen__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__0"


    // $ANTLR start "rule__VideoGen__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:354:1: rule__VideoGen__Group__0__Impl : ( () ) ;
    public final void rule__VideoGen__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:358:1: ( ( () ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:359:1: ( () )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:359:1: ( () )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:360:1: ()
            {
             before(grammarAccess.getVideoGenAccess().getVideoGenAction_0()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:361:1: ()
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:363:1: 
            {
            }

             after(grammarAccess.getVideoGenAccess().getVideoGenAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__0__Impl"


    // $ANTLR start "rule__VideoGen__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:373:1: rule__VideoGen__Group__1 : rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2 ;
    public final void rule__VideoGen__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:377:1: ( rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:378:2: rule__VideoGen__Group__1__Impl rule__VideoGen__Group__2
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__1__Impl_in_rule__VideoGen__Group__1735);
            rule__VideoGen__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__2_in_rule__VideoGen__Group__1738);
            rule__VideoGen__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__1"


    // $ANTLR start "rule__VideoGen__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:385:1: rule__VideoGen__Group__1__Impl : ( 'VideoGen' ) ;
    public final void rule__VideoGen__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:389:1: ( ( 'VideoGen' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:390:1: ( 'VideoGen' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:390:1: ( 'VideoGen' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:391:1: 'VideoGen'
            {
             before(grammarAccess.getVideoGenAccess().getVideoGenKeyword_1()); 
            match(input,11,FOLLOW_11_in_rule__VideoGen__Group__1__Impl766); 
             after(grammarAccess.getVideoGenAccess().getVideoGenKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__1__Impl"


    // $ANTLR start "rule__VideoGen__Group__2"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:404:1: rule__VideoGen__Group__2 : rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3 ;
    public final void rule__VideoGen__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:408:1: ( rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:409:2: rule__VideoGen__Group__2__Impl rule__VideoGen__Group__3
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__2__Impl_in_rule__VideoGen__Group__2797);
            rule__VideoGen__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__3_in_rule__VideoGen__Group__2800);
            rule__VideoGen__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__2"


    // $ANTLR start "rule__VideoGen__Group__2__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:416:1: rule__VideoGen__Group__2__Impl : ( '{' ) ;
    public final void rule__VideoGen__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:420:1: ( ( '{' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:421:1: ( '{' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:421:1: ( '{' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:422:1: '{'
            {
             before(grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__VideoGen__Group__2__Impl828); 
             after(grammarAccess.getVideoGenAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__2__Impl"


    // $ANTLR start "rule__VideoGen__Group__3"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:435:1: rule__VideoGen__Group__3 : rule__VideoGen__Group__3__Impl rule__VideoGen__Group__4 ;
    public final void rule__VideoGen__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:439:1: ( rule__VideoGen__Group__3__Impl rule__VideoGen__Group__4 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:440:2: rule__VideoGen__Group__3__Impl rule__VideoGen__Group__4
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__3__Impl_in_rule__VideoGen__Group__3859);
            rule__VideoGen__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoGen__Group__4_in_rule__VideoGen__Group__3862);
            rule__VideoGen__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__3"


    // $ANTLR start "rule__VideoGen__Group__3__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:447:1: rule__VideoGen__Group__3__Impl : ( ( rule__VideoGen__VideoAssignment_3 )* ) ;
    public final void rule__VideoGen__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:451:1: ( ( ( rule__VideoGen__VideoAssignment_3 )* ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:452:1: ( ( rule__VideoGen__VideoAssignment_3 )* )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:452:1: ( ( rule__VideoGen__VideoAssignment_3 )* )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:453:1: ( rule__VideoGen__VideoAssignment_3 )*
            {
             before(grammarAccess.getVideoGenAccess().getVideoAssignment_3()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:454:1: ( rule__VideoGen__VideoAssignment_3 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=14 && LA2_0<=16)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:454:2: rule__VideoGen__VideoAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__VideoGen__VideoAssignment_3_in_rule__VideoGen__Group__3__Impl889);
            	    rule__VideoGen__VideoAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getVideoGenAccess().getVideoAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__3__Impl"


    // $ANTLR start "rule__VideoGen__Group__4"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:464:1: rule__VideoGen__Group__4 : rule__VideoGen__Group__4__Impl ;
    public final void rule__VideoGen__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:468:1: ( rule__VideoGen__Group__4__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:469:2: rule__VideoGen__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__VideoGen__Group__4__Impl_in_rule__VideoGen__Group__4920);
            rule__VideoGen__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__4"


    // $ANTLR start "rule__VideoGen__Group__4__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:475:1: rule__VideoGen__Group__4__Impl : ( '}' ) ;
    public final void rule__VideoGen__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:479:1: ( ( '}' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:480:1: ( '}' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:480:1: ( '}' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:481:1: '}'
            {
             before(grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_4()); 
            match(input,13,FOLLOW_13_in_rule__VideoGen__Group__4__Impl948); 
             after(grammarAccess.getVideoGenAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__Group__4__Impl"


    // $ANTLR start "rule__Mandatory__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:504:1: rule__Mandatory__Group__0 : rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1 ;
    public final void rule__Mandatory__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:508:1: ( rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:509:2: rule__Mandatory__Group__0__Impl rule__Mandatory__Group__1
            {
            pushFollow(FOLLOW_rule__Mandatory__Group__0__Impl_in_rule__Mandatory__Group__0989);
            rule__Mandatory__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Mandatory__Group__1_in_rule__Mandatory__Group__0992);
            rule__Mandatory__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__0"


    // $ANTLR start "rule__Mandatory__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:516:1: rule__Mandatory__Group__0__Impl : ( 'mandatory' ) ;
    public final void rule__Mandatory__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:520:1: ( ( 'mandatory' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:521:1: ( 'mandatory' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:521:1: ( 'mandatory' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:522:1: 'mandatory'
            {
             before(grammarAccess.getMandatoryAccess().getMandatoryKeyword_0()); 
            match(input,14,FOLLOW_14_in_rule__Mandatory__Group__0__Impl1020); 
             after(grammarAccess.getMandatoryAccess().getMandatoryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__0__Impl"


    // $ANTLR start "rule__Mandatory__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:535:1: rule__Mandatory__Group__1 : rule__Mandatory__Group__1__Impl ;
    public final void rule__Mandatory__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:539:1: ( rule__Mandatory__Group__1__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:540:2: rule__Mandatory__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Mandatory__Group__1__Impl_in_rule__Mandatory__Group__11051);
            rule__Mandatory__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__1"


    // $ANTLR start "rule__Mandatory__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:546:1: rule__Mandatory__Group__1__Impl : ( ( rule__Mandatory__VideoSeqAssignment_1 ) ) ;
    public final void rule__Mandatory__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:550:1: ( ( ( rule__Mandatory__VideoSeqAssignment_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:551:1: ( ( rule__Mandatory__VideoSeqAssignment_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:551:1: ( ( rule__Mandatory__VideoSeqAssignment_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:552:1: ( rule__Mandatory__VideoSeqAssignment_1 )
            {
             before(grammarAccess.getMandatoryAccess().getVideoSeqAssignment_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:553:1: ( rule__Mandatory__VideoSeqAssignment_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:553:2: rule__Mandatory__VideoSeqAssignment_1
            {
            pushFollow(FOLLOW_rule__Mandatory__VideoSeqAssignment_1_in_rule__Mandatory__Group__1__Impl1078);
            rule__Mandatory__VideoSeqAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMandatoryAccess().getVideoSeqAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__Group__1__Impl"


    // $ANTLR start "rule__Optional__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:567:1: rule__Optional__Group__0 : rule__Optional__Group__0__Impl rule__Optional__Group__1 ;
    public final void rule__Optional__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:571:1: ( rule__Optional__Group__0__Impl rule__Optional__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:572:2: rule__Optional__Group__0__Impl rule__Optional__Group__1
            {
            pushFollow(FOLLOW_rule__Optional__Group__0__Impl_in_rule__Optional__Group__01112);
            rule__Optional__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Optional__Group__1_in_rule__Optional__Group__01115);
            rule__Optional__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__0"


    // $ANTLR start "rule__Optional__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:579:1: rule__Optional__Group__0__Impl : ( 'Optional' ) ;
    public final void rule__Optional__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:583:1: ( ( 'Optional' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:584:1: ( 'Optional' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:584:1: ( 'Optional' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:585:1: 'Optional'
            {
             before(grammarAccess.getOptionalAccess().getOptionalKeyword_0()); 
            match(input,15,FOLLOW_15_in_rule__Optional__Group__0__Impl1143); 
             after(grammarAccess.getOptionalAccess().getOptionalKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__0__Impl"


    // $ANTLR start "rule__Optional__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:598:1: rule__Optional__Group__1 : rule__Optional__Group__1__Impl ;
    public final void rule__Optional__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:602:1: ( rule__Optional__Group__1__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:603:2: rule__Optional__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Optional__Group__1__Impl_in_rule__Optional__Group__11174);
            rule__Optional__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__1"


    // $ANTLR start "rule__Optional__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:609:1: rule__Optional__Group__1__Impl : ( ( rule__Optional__VideoSeqAssignment_1 ) ) ;
    public final void rule__Optional__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:613:1: ( ( ( rule__Optional__VideoSeqAssignment_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:614:1: ( ( rule__Optional__VideoSeqAssignment_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:614:1: ( ( rule__Optional__VideoSeqAssignment_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:615:1: ( rule__Optional__VideoSeqAssignment_1 )
            {
             before(grammarAccess.getOptionalAccess().getVideoSeqAssignment_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:616:1: ( rule__Optional__VideoSeqAssignment_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:616:2: rule__Optional__VideoSeqAssignment_1
            {
            pushFollow(FOLLOW_rule__Optional__VideoSeqAssignment_1_in_rule__Optional__Group__1__Impl1201);
            rule__Optional__VideoSeqAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionalAccess().getVideoSeqAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__Group__1__Impl"


    // $ANTLR start "rule__Alternative__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:630:1: rule__Alternative__Group__0 : rule__Alternative__Group__0__Impl rule__Alternative__Group__1 ;
    public final void rule__Alternative__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:634:1: ( rule__Alternative__Group__0__Impl rule__Alternative__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:635:2: rule__Alternative__Group__0__Impl rule__Alternative__Group__1
            {
            pushFollow(FOLLOW_rule__Alternative__Group__0__Impl_in_rule__Alternative__Group__01235);
            rule__Alternative__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternative__Group__1_in_rule__Alternative__Group__01238);
            rule__Alternative__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__0"


    // $ANTLR start "rule__Alternative__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:642:1: rule__Alternative__Group__0__Impl : ( 'Alternative' ) ;
    public final void rule__Alternative__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:646:1: ( ( 'Alternative' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:647:1: ( 'Alternative' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:647:1: ( 'Alternative' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:648:1: 'Alternative'
            {
             before(grammarAccess.getAlternativeAccess().getAlternativeKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__Alternative__Group__0__Impl1266); 
             after(grammarAccess.getAlternativeAccess().getAlternativeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__0__Impl"


    // $ANTLR start "rule__Alternative__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:661:1: rule__Alternative__Group__1 : rule__Alternative__Group__1__Impl rule__Alternative__Group__2 ;
    public final void rule__Alternative__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:665:1: ( rule__Alternative__Group__1__Impl rule__Alternative__Group__2 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:666:2: rule__Alternative__Group__1__Impl rule__Alternative__Group__2
            {
            pushFollow(FOLLOW_rule__Alternative__Group__1__Impl_in_rule__Alternative__Group__11297);
            rule__Alternative__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternative__Group__2_in_rule__Alternative__Group__11300);
            rule__Alternative__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__1"


    // $ANTLR start "rule__Alternative__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:673:1: rule__Alternative__Group__1__Impl : ( ( rule__Alternative__NameAssignment_1 )? ) ;
    public final void rule__Alternative__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:677:1: ( ( ( rule__Alternative__NameAssignment_1 )? ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:678:1: ( ( rule__Alternative__NameAssignment_1 )? )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:678:1: ( ( rule__Alternative__NameAssignment_1 )? )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:679:1: ( rule__Alternative__NameAssignment_1 )?
            {
             before(grammarAccess.getAlternativeAccess().getNameAssignment_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:680:1: ( rule__Alternative__NameAssignment_1 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:680:2: rule__Alternative__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Alternative__NameAssignment_1_in_rule__Alternative__Group__1__Impl1327);
                    rule__Alternative__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAlternativeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__1__Impl"


    // $ANTLR start "rule__Alternative__Group__2"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:690:1: rule__Alternative__Group__2 : rule__Alternative__Group__2__Impl rule__Alternative__Group__3 ;
    public final void rule__Alternative__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:694:1: ( rule__Alternative__Group__2__Impl rule__Alternative__Group__3 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:695:2: rule__Alternative__Group__2__Impl rule__Alternative__Group__3
            {
            pushFollow(FOLLOW_rule__Alternative__Group__2__Impl_in_rule__Alternative__Group__21358);
            rule__Alternative__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternative__Group__3_in_rule__Alternative__Group__21361);
            rule__Alternative__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__2"


    // $ANTLR start "rule__Alternative__Group__2__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:702:1: rule__Alternative__Group__2__Impl : ( '{' ) ;
    public final void rule__Alternative__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:706:1: ( ( '{' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:707:1: ( '{' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:707:1: ( '{' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:708:1: '{'
            {
             before(grammarAccess.getAlternativeAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__Alternative__Group__2__Impl1389); 
             after(grammarAccess.getAlternativeAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__2__Impl"


    // $ANTLR start "rule__Alternative__Group__3"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:721:1: rule__Alternative__Group__3 : rule__Alternative__Group__3__Impl rule__Alternative__Group__4 ;
    public final void rule__Alternative__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:725:1: ( rule__Alternative__Group__3__Impl rule__Alternative__Group__4 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:726:2: rule__Alternative__Group__3__Impl rule__Alternative__Group__4
            {
            pushFollow(FOLLOW_rule__Alternative__Group__3__Impl_in_rule__Alternative__Group__31420);
            rule__Alternative__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alternative__Group__4_in_rule__Alternative__Group__31423);
            rule__Alternative__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__3"


    // $ANTLR start "rule__Alternative__Group__3__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:733:1: rule__Alternative__Group__3__Impl : ( ( ( rule__Alternative__VideoSeqAssignment_3 ) ) ( ( rule__Alternative__VideoSeqAssignment_3 )* ) ) ;
    public final void rule__Alternative__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:737:1: ( ( ( ( rule__Alternative__VideoSeqAssignment_3 ) ) ( ( rule__Alternative__VideoSeqAssignment_3 )* ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:738:1: ( ( ( rule__Alternative__VideoSeqAssignment_3 ) ) ( ( rule__Alternative__VideoSeqAssignment_3 )* ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:738:1: ( ( ( rule__Alternative__VideoSeqAssignment_3 ) ) ( ( rule__Alternative__VideoSeqAssignment_3 )* ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:739:1: ( ( rule__Alternative__VideoSeqAssignment_3 ) ) ( ( rule__Alternative__VideoSeqAssignment_3 )* )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:739:1: ( ( rule__Alternative__VideoSeqAssignment_3 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:740:1: ( rule__Alternative__VideoSeqAssignment_3 )
            {
             before(grammarAccess.getAlternativeAccess().getVideoSeqAssignment_3()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:741:1: ( rule__Alternative__VideoSeqAssignment_3 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:741:2: rule__Alternative__VideoSeqAssignment_3
            {
            pushFollow(FOLLOW_rule__Alternative__VideoSeqAssignment_3_in_rule__Alternative__Group__3__Impl1452);
            rule__Alternative__VideoSeqAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAlternativeAccess().getVideoSeqAssignment_3()); 

            }

            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:744:1: ( ( rule__Alternative__VideoSeqAssignment_3 )* )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:745:1: ( rule__Alternative__VideoSeqAssignment_3 )*
            {
             before(grammarAccess.getAlternativeAccess().getVideoSeqAssignment_3()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:746:1: ( rule__Alternative__VideoSeqAssignment_3 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:746:2: rule__Alternative__VideoSeqAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Alternative__VideoSeqAssignment_3_in_rule__Alternative__Group__3__Impl1464);
            	    rule__Alternative__VideoSeqAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getAlternativeAccess().getVideoSeqAssignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__3__Impl"


    // $ANTLR start "rule__Alternative__Group__4"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:757:1: rule__Alternative__Group__4 : rule__Alternative__Group__4__Impl ;
    public final void rule__Alternative__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:761:1: ( rule__Alternative__Group__4__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:762:2: rule__Alternative__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Alternative__Group__4__Impl_in_rule__Alternative__Group__41497);
            rule__Alternative__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__4"


    // $ANTLR start "rule__Alternative__Group__4__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:768:1: rule__Alternative__Group__4__Impl : ( '}' ) ;
    public final void rule__Alternative__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:772:1: ( ( '}' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:773:1: ( '}' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:773:1: ( '}' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:774:1: '}'
            {
             before(grammarAccess.getAlternativeAccess().getRightCurlyBracketKeyword_4()); 
            match(input,13,FOLLOW_13_in_rule__Alternative__Group__4__Impl1525); 
             after(grammarAccess.getAlternativeAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__Group__4__Impl"


    // $ANTLR start "rule__VideoSeq__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:797:1: rule__VideoSeq__Group__0 : rule__VideoSeq__Group__0__Impl rule__VideoSeq__Group__1 ;
    public final void rule__VideoSeq__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:801:1: ( rule__VideoSeq__Group__0__Impl rule__VideoSeq__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:802:2: rule__VideoSeq__Group__0__Impl rule__VideoSeq__Group__1
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group__0__Impl_in_rule__VideoSeq__Group__01566);
            rule__VideoSeq__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group__1_in_rule__VideoSeq__Group__01569);
            rule__VideoSeq__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__0"


    // $ANTLR start "rule__VideoSeq__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:809:1: rule__VideoSeq__Group__0__Impl : ( 'VideoSeq' ) ;
    public final void rule__VideoSeq__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:813:1: ( ( 'VideoSeq' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:814:1: ( 'VideoSeq' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:814:1: ( 'VideoSeq' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:815:1: 'VideoSeq'
            {
             before(grammarAccess.getVideoSeqAccess().getVideoSeqKeyword_0()); 
            match(input,17,FOLLOW_17_in_rule__VideoSeq__Group__0__Impl1597); 
             after(grammarAccess.getVideoSeqAccess().getVideoSeqKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__0__Impl"


    // $ANTLR start "rule__VideoSeq__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:828:1: rule__VideoSeq__Group__1 : rule__VideoSeq__Group__1__Impl rule__VideoSeq__Group__2 ;
    public final void rule__VideoSeq__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:832:1: ( rule__VideoSeq__Group__1__Impl rule__VideoSeq__Group__2 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:833:2: rule__VideoSeq__Group__1__Impl rule__VideoSeq__Group__2
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group__1__Impl_in_rule__VideoSeq__Group__11628);
            rule__VideoSeq__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group__2_in_rule__VideoSeq__Group__11631);
            rule__VideoSeq__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__1"


    // $ANTLR start "rule__VideoSeq__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:840:1: rule__VideoSeq__Group__1__Impl : ( ( rule__VideoSeq__NameAssignment_1 )? ) ;
    public final void rule__VideoSeq__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:844:1: ( ( ( rule__VideoSeq__NameAssignment_1 )? ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:845:1: ( ( rule__VideoSeq__NameAssignment_1 )? )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:845:1: ( ( rule__VideoSeq__NameAssignment_1 )? )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:846:1: ( rule__VideoSeq__NameAssignment_1 )?
            {
             before(grammarAccess.getVideoSeqAccess().getNameAssignment_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:847:1: ( rule__VideoSeq__NameAssignment_1 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:847:2: rule__VideoSeq__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__VideoSeq__NameAssignment_1_in_rule__VideoSeq__Group__1__Impl1658);
                    rule__VideoSeq__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVideoSeqAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__1__Impl"


    // $ANTLR start "rule__VideoSeq__Group__2"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:857:1: rule__VideoSeq__Group__2 : rule__VideoSeq__Group__2__Impl rule__VideoSeq__Group__3 ;
    public final void rule__VideoSeq__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:861:1: ( rule__VideoSeq__Group__2__Impl rule__VideoSeq__Group__3 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:862:2: rule__VideoSeq__Group__2__Impl rule__VideoSeq__Group__3
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group__2__Impl_in_rule__VideoSeq__Group__21689);
            rule__VideoSeq__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group__3_in_rule__VideoSeq__Group__21692);
            rule__VideoSeq__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__2"


    // $ANTLR start "rule__VideoSeq__Group__2__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:869:1: rule__VideoSeq__Group__2__Impl : ( ( rule__VideoSeq__TextAssignment_2 ) ) ;
    public final void rule__VideoSeq__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:873:1: ( ( ( rule__VideoSeq__TextAssignment_2 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:874:1: ( ( rule__VideoSeq__TextAssignment_2 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:874:1: ( ( rule__VideoSeq__TextAssignment_2 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:875:1: ( rule__VideoSeq__TextAssignment_2 )
            {
             before(grammarAccess.getVideoSeqAccess().getTextAssignment_2()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:876:1: ( rule__VideoSeq__TextAssignment_2 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:876:2: rule__VideoSeq__TextAssignment_2
            {
            pushFollow(FOLLOW_rule__VideoSeq__TextAssignment_2_in_rule__VideoSeq__Group__2__Impl1719);
            rule__VideoSeq__TextAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVideoSeqAccess().getTextAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__2__Impl"


    // $ANTLR start "rule__VideoSeq__Group__3"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:886:1: rule__VideoSeq__Group__3 : rule__VideoSeq__Group__3__Impl rule__VideoSeq__Group__4 ;
    public final void rule__VideoSeq__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:890:1: ( rule__VideoSeq__Group__3__Impl rule__VideoSeq__Group__4 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:891:2: rule__VideoSeq__Group__3__Impl rule__VideoSeq__Group__4
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group__3__Impl_in_rule__VideoSeq__Group__31749);
            rule__VideoSeq__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group__4_in_rule__VideoSeq__Group__31752);
            rule__VideoSeq__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__3"


    // $ANTLR start "rule__VideoSeq__Group__3__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:898:1: rule__VideoSeq__Group__3__Impl : ( ( rule__VideoSeq__Group_3__0 )? ) ;
    public final void rule__VideoSeq__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:902:1: ( ( ( rule__VideoSeq__Group_3__0 )? ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:903:1: ( ( rule__VideoSeq__Group_3__0 )? )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:903:1: ( ( rule__VideoSeq__Group_3__0 )? )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:904:1: ( rule__VideoSeq__Group_3__0 )?
            {
             before(grammarAccess.getVideoSeqAccess().getGroup_3()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:905:1: ( rule__VideoSeq__Group_3__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==19) ) {
                    alt6=1;
                }
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:905:2: rule__VideoSeq__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__VideoSeq__Group_3__0_in_rule__VideoSeq__Group__3__Impl1779);
                    rule__VideoSeq__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVideoSeqAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__3__Impl"


    // $ANTLR start "rule__VideoSeq__Group__4"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:915:1: rule__VideoSeq__Group__4 : rule__VideoSeq__Group__4__Impl rule__VideoSeq__Group__5 ;
    public final void rule__VideoSeq__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:919:1: ( rule__VideoSeq__Group__4__Impl rule__VideoSeq__Group__5 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:920:2: rule__VideoSeq__Group__4__Impl rule__VideoSeq__Group__5
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group__4__Impl_in_rule__VideoSeq__Group__41810);
            rule__VideoSeq__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group__5_in_rule__VideoSeq__Group__41813);
            rule__VideoSeq__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__4"


    // $ANTLR start "rule__VideoSeq__Group__4__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:927:1: rule__VideoSeq__Group__4__Impl : ( ( rule__VideoSeq__Group_4__0 )? ) ;
    public final void rule__VideoSeq__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:931:1: ( ( ( rule__VideoSeq__Group_4__0 )? ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:932:1: ( ( rule__VideoSeq__Group_4__0 )? )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:932:1: ( ( rule__VideoSeq__Group_4__0 )? )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:933:1: ( rule__VideoSeq__Group_4__0 )?
            {
             before(grammarAccess.getVideoSeqAccess().getGroup_4()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:934:1: ( rule__VideoSeq__Group_4__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==18) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==20) ) {
                    alt7=1;
                }
            }
            switch (alt7) {
                case 1 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:934:2: rule__VideoSeq__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__VideoSeq__Group_4__0_in_rule__VideoSeq__Group__4__Impl1840);
                    rule__VideoSeq__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVideoSeqAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__4__Impl"


    // $ANTLR start "rule__VideoSeq__Group__5"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:944:1: rule__VideoSeq__Group__5 : rule__VideoSeq__Group__5__Impl ;
    public final void rule__VideoSeq__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:948:1: ( rule__VideoSeq__Group__5__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:949:2: rule__VideoSeq__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group__5__Impl_in_rule__VideoSeq__Group__51871);
            rule__VideoSeq__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__5"


    // $ANTLR start "rule__VideoSeq__Group__5__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:955:1: rule__VideoSeq__Group__5__Impl : ( ( rule__VideoSeq__Group_5__0 )? ) ;
    public final void rule__VideoSeq__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:959:1: ( ( ( rule__VideoSeq__Group_5__0 )? ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:960:1: ( ( rule__VideoSeq__Group_5__0 )? )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:960:1: ( ( rule__VideoSeq__Group_5__0 )? )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:961:1: ( rule__VideoSeq__Group_5__0 )?
            {
             before(grammarAccess.getVideoSeqAccess().getGroup_5()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:962:1: ( rule__VideoSeq__Group_5__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==18) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:962:2: rule__VideoSeq__Group_5__0
                    {
                    pushFollow(FOLLOW_rule__VideoSeq__Group_5__0_in_rule__VideoSeq__Group__5__Impl1898);
                    rule__VideoSeq__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVideoSeqAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group__5__Impl"


    // $ANTLR start "rule__VideoSeq__Group_3__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:984:1: rule__VideoSeq__Group_3__0 : rule__VideoSeq__Group_3__0__Impl rule__VideoSeq__Group_3__1 ;
    public final void rule__VideoSeq__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:988:1: ( rule__VideoSeq__Group_3__0__Impl rule__VideoSeq__Group_3__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:989:2: rule__VideoSeq__Group_3__0__Impl rule__VideoSeq__Group_3__1
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group_3__0__Impl_in_rule__VideoSeq__Group_3__01941);
            rule__VideoSeq__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group_3__1_in_rule__VideoSeq__Group_3__01944);
            rule__VideoSeq__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_3__0"


    // $ANTLR start "rule__VideoSeq__Group_3__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:996:1: rule__VideoSeq__Group_3__0__Impl : ( ',' ) ;
    public final void rule__VideoSeq__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1000:1: ( ( ',' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1001:1: ( ',' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1001:1: ( ',' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1002:1: ','
            {
             before(grammarAccess.getVideoSeqAccess().getCommaKeyword_3_0()); 
            match(input,18,FOLLOW_18_in_rule__VideoSeq__Group_3__0__Impl1972); 
             after(grammarAccess.getVideoSeqAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_3__0__Impl"


    // $ANTLR start "rule__VideoSeq__Group_3__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1015:1: rule__VideoSeq__Group_3__1 : rule__VideoSeq__Group_3__1__Impl ;
    public final void rule__VideoSeq__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1019:1: ( rule__VideoSeq__Group_3__1__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1020:2: rule__VideoSeq__Group_3__1__Impl
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group_3__1__Impl_in_rule__VideoSeq__Group_3__12003);
            rule__VideoSeq__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_3__1"


    // $ANTLR start "rule__VideoSeq__Group_3__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1026:1: rule__VideoSeq__Group_3__1__Impl : ( ( rule__VideoSeq__DescriptionAssignment_3_1 ) ) ;
    public final void rule__VideoSeq__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1030:1: ( ( ( rule__VideoSeq__DescriptionAssignment_3_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1031:1: ( ( rule__VideoSeq__DescriptionAssignment_3_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1031:1: ( ( rule__VideoSeq__DescriptionAssignment_3_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1032:1: ( rule__VideoSeq__DescriptionAssignment_3_1 )
            {
             before(grammarAccess.getVideoSeqAccess().getDescriptionAssignment_3_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1033:1: ( rule__VideoSeq__DescriptionAssignment_3_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1033:2: rule__VideoSeq__DescriptionAssignment_3_1
            {
            pushFollow(FOLLOW_rule__VideoSeq__DescriptionAssignment_3_1_in_rule__VideoSeq__Group_3__1__Impl2030);
            rule__VideoSeq__DescriptionAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSeqAccess().getDescriptionAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_3__1__Impl"


    // $ANTLR start "rule__VideoSeq__Group_4__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1047:1: rule__VideoSeq__Group_4__0 : rule__VideoSeq__Group_4__0__Impl rule__VideoSeq__Group_4__1 ;
    public final void rule__VideoSeq__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1051:1: ( rule__VideoSeq__Group_4__0__Impl rule__VideoSeq__Group_4__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1052:2: rule__VideoSeq__Group_4__0__Impl rule__VideoSeq__Group_4__1
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group_4__0__Impl_in_rule__VideoSeq__Group_4__02064);
            rule__VideoSeq__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group_4__1_in_rule__VideoSeq__Group_4__02067);
            rule__VideoSeq__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_4__0"


    // $ANTLR start "rule__VideoSeq__Group_4__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1059:1: rule__VideoSeq__Group_4__0__Impl : ( ',' ) ;
    public final void rule__VideoSeq__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1063:1: ( ( ',' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1064:1: ( ',' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1064:1: ( ',' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1065:1: ','
            {
             before(grammarAccess.getVideoSeqAccess().getCommaKeyword_4_0()); 
            match(input,18,FOLLOW_18_in_rule__VideoSeq__Group_4__0__Impl2095); 
             after(grammarAccess.getVideoSeqAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_4__0__Impl"


    // $ANTLR start "rule__VideoSeq__Group_4__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1078:1: rule__VideoSeq__Group_4__1 : rule__VideoSeq__Group_4__1__Impl ;
    public final void rule__VideoSeq__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1082:1: ( rule__VideoSeq__Group_4__1__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1083:2: rule__VideoSeq__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group_4__1__Impl_in_rule__VideoSeq__Group_4__12126);
            rule__VideoSeq__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_4__1"


    // $ANTLR start "rule__VideoSeq__Group_4__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1089:1: rule__VideoSeq__Group_4__1__Impl : ( ( rule__VideoSeq__DureeAssignment_4_1 ) ) ;
    public final void rule__VideoSeq__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1093:1: ( ( ( rule__VideoSeq__DureeAssignment_4_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1094:1: ( ( rule__VideoSeq__DureeAssignment_4_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1094:1: ( ( rule__VideoSeq__DureeAssignment_4_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1095:1: ( rule__VideoSeq__DureeAssignment_4_1 )
            {
             before(grammarAccess.getVideoSeqAccess().getDureeAssignment_4_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1096:1: ( rule__VideoSeq__DureeAssignment_4_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1096:2: rule__VideoSeq__DureeAssignment_4_1
            {
            pushFollow(FOLLOW_rule__VideoSeq__DureeAssignment_4_1_in_rule__VideoSeq__Group_4__1__Impl2153);
            rule__VideoSeq__DureeAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSeqAccess().getDureeAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_4__1__Impl"


    // $ANTLR start "rule__VideoSeq__Group_5__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1110:1: rule__VideoSeq__Group_5__0 : rule__VideoSeq__Group_5__0__Impl rule__VideoSeq__Group_5__1 ;
    public final void rule__VideoSeq__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1114:1: ( rule__VideoSeq__Group_5__0__Impl rule__VideoSeq__Group_5__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1115:2: rule__VideoSeq__Group_5__0__Impl rule__VideoSeq__Group_5__1
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group_5__0__Impl_in_rule__VideoSeq__Group_5__02187);
            rule__VideoSeq__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VideoSeq__Group_5__1_in_rule__VideoSeq__Group_5__02190);
            rule__VideoSeq__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_5__0"


    // $ANTLR start "rule__VideoSeq__Group_5__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1122:1: rule__VideoSeq__Group_5__0__Impl : ( ',' ) ;
    public final void rule__VideoSeq__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1126:1: ( ( ',' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1127:1: ( ',' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1127:1: ( ',' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1128:1: ','
            {
             before(grammarAccess.getVideoSeqAccess().getCommaKeyword_5_0()); 
            match(input,18,FOLLOW_18_in_rule__VideoSeq__Group_5__0__Impl2218); 
             after(grammarAccess.getVideoSeqAccess().getCommaKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_5__0__Impl"


    // $ANTLR start "rule__VideoSeq__Group_5__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1141:1: rule__VideoSeq__Group_5__1 : rule__VideoSeq__Group_5__1__Impl ;
    public final void rule__VideoSeq__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1145:1: ( rule__VideoSeq__Group_5__1__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1146:2: rule__VideoSeq__Group_5__1__Impl
            {
            pushFollow(FOLLOW_rule__VideoSeq__Group_5__1__Impl_in_rule__VideoSeq__Group_5__12249);
            rule__VideoSeq__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_5__1"


    // $ANTLR start "rule__VideoSeq__Group_5__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1152:1: rule__VideoSeq__Group_5__1__Impl : ( ( rule__VideoSeq__ProbabilteAssignment_5_1 ) ) ;
    public final void rule__VideoSeq__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1156:1: ( ( ( rule__VideoSeq__ProbabilteAssignment_5_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1157:1: ( ( rule__VideoSeq__ProbabilteAssignment_5_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1157:1: ( ( rule__VideoSeq__ProbabilteAssignment_5_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1158:1: ( rule__VideoSeq__ProbabilteAssignment_5_1 )
            {
             before(grammarAccess.getVideoSeqAccess().getProbabilteAssignment_5_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1159:1: ( rule__VideoSeq__ProbabilteAssignment_5_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1159:2: rule__VideoSeq__ProbabilteAssignment_5_1
            {
            pushFollow(FOLLOW_rule__VideoSeq__ProbabilteAssignment_5_1_in_rule__VideoSeq__Group_5__1__Impl2276);
            rule__VideoSeq__ProbabilteAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getVideoSeqAccess().getProbabilteAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__Group_5__1__Impl"


    // $ANTLR start "rule__Description__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1173:1: rule__Description__Group__0 : rule__Description__Group__0__Impl rule__Description__Group__1 ;
    public final void rule__Description__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1177:1: ( rule__Description__Group__0__Impl rule__Description__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1178:2: rule__Description__Group__0__Impl rule__Description__Group__1
            {
            pushFollow(FOLLOW_rule__Description__Group__0__Impl_in_rule__Description__Group__02310);
            rule__Description__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Description__Group__1_in_rule__Description__Group__02313);
            rule__Description__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0"


    // $ANTLR start "rule__Description__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1185:1: rule__Description__Group__0__Impl : ( 'Description' ) ;
    public final void rule__Description__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1189:1: ( ( 'Description' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1190:1: ( 'Description' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1190:1: ( 'Description' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1191:1: 'Description'
            {
             before(grammarAccess.getDescriptionAccess().getDescriptionKeyword_0()); 
            match(input,19,FOLLOW_19_in_rule__Description__Group__0__Impl2341); 
             after(grammarAccess.getDescriptionAccess().getDescriptionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__0__Impl"


    // $ANTLR start "rule__Description__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1204:1: rule__Description__Group__1 : rule__Description__Group__1__Impl ;
    public final void rule__Description__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1208:1: ( rule__Description__Group__1__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1209:2: rule__Description__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Description__Group__1__Impl_in_rule__Description__Group__12372);
            rule__Description__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1"


    // $ANTLR start "rule__Description__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1215:1: rule__Description__Group__1__Impl : ( ( rule__Description__TextAssignment_1 ) ) ;
    public final void rule__Description__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1219:1: ( ( ( rule__Description__TextAssignment_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1220:1: ( ( rule__Description__TextAssignment_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1220:1: ( ( rule__Description__TextAssignment_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1221:1: ( rule__Description__TextAssignment_1 )
            {
             before(grammarAccess.getDescriptionAccess().getTextAssignment_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1222:1: ( rule__Description__TextAssignment_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1222:2: rule__Description__TextAssignment_1
            {
            pushFollow(FOLLOW_rule__Description__TextAssignment_1_in_rule__Description__Group__1__Impl2399);
            rule__Description__TextAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDescriptionAccess().getTextAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__Group__1__Impl"


    // $ANTLR start "rule__Duree__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1236:1: rule__Duree__Group__0 : rule__Duree__Group__0__Impl rule__Duree__Group__1 ;
    public final void rule__Duree__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1240:1: ( rule__Duree__Group__0__Impl rule__Duree__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1241:2: rule__Duree__Group__0__Impl rule__Duree__Group__1
            {
            pushFollow(FOLLOW_rule__Duree__Group__0__Impl_in_rule__Duree__Group__02433);
            rule__Duree__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Duree__Group__1_in_rule__Duree__Group__02436);
            rule__Duree__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duree__Group__0"


    // $ANTLR start "rule__Duree__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1248:1: rule__Duree__Group__0__Impl : ( 'Duree' ) ;
    public final void rule__Duree__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1252:1: ( ( 'Duree' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1253:1: ( 'Duree' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1253:1: ( 'Duree' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1254:1: 'Duree'
            {
             before(grammarAccess.getDureeAccess().getDureeKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__Duree__Group__0__Impl2464); 
             after(grammarAccess.getDureeAccess().getDureeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duree__Group__0__Impl"


    // $ANTLR start "rule__Duree__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1267:1: rule__Duree__Group__1 : rule__Duree__Group__1__Impl ;
    public final void rule__Duree__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1271:1: ( rule__Duree__Group__1__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1272:2: rule__Duree__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Duree__Group__1__Impl_in_rule__Duree__Group__12495);
            rule__Duree__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duree__Group__1"


    // $ANTLR start "rule__Duree__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1278:1: rule__Duree__Group__1__Impl : ( ( rule__Duree__TempsAssignment_1 ) ) ;
    public final void rule__Duree__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1282:1: ( ( ( rule__Duree__TempsAssignment_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1283:1: ( ( rule__Duree__TempsAssignment_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1283:1: ( ( rule__Duree__TempsAssignment_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1284:1: ( rule__Duree__TempsAssignment_1 )
            {
             before(grammarAccess.getDureeAccess().getTempsAssignment_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1285:1: ( rule__Duree__TempsAssignment_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1285:2: rule__Duree__TempsAssignment_1
            {
            pushFollow(FOLLOW_rule__Duree__TempsAssignment_1_in_rule__Duree__Group__1__Impl2522);
            rule__Duree__TempsAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDureeAccess().getTempsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duree__Group__1__Impl"


    // $ANTLR start "rule__Probabilte__Group__0"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1299:1: rule__Probabilte__Group__0 : rule__Probabilte__Group__0__Impl rule__Probabilte__Group__1 ;
    public final void rule__Probabilte__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1303:1: ( rule__Probabilte__Group__0__Impl rule__Probabilte__Group__1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1304:2: rule__Probabilte__Group__0__Impl rule__Probabilte__Group__1
            {
            pushFollow(FOLLOW_rule__Probabilte__Group__0__Impl_in_rule__Probabilte__Group__02556);
            rule__Probabilte__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Probabilte__Group__1_in_rule__Probabilte__Group__02559);
            rule__Probabilte__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Probabilte__Group__0"


    // $ANTLR start "rule__Probabilte__Group__0__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1311:1: rule__Probabilte__Group__0__Impl : ( 'Probabilte' ) ;
    public final void rule__Probabilte__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1315:1: ( ( 'Probabilte' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1316:1: ( 'Probabilte' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1316:1: ( 'Probabilte' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1317:1: 'Probabilte'
            {
             before(grammarAccess.getProbabilteAccess().getProbabilteKeyword_0()); 
            match(input,21,FOLLOW_21_in_rule__Probabilte__Group__0__Impl2587); 
             after(grammarAccess.getProbabilteAccess().getProbabilteKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Probabilte__Group__0__Impl"


    // $ANTLR start "rule__Probabilte__Group__1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1330:1: rule__Probabilte__Group__1 : rule__Probabilte__Group__1__Impl rule__Probabilte__Group__2 ;
    public final void rule__Probabilte__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1334:1: ( rule__Probabilte__Group__1__Impl rule__Probabilte__Group__2 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1335:2: rule__Probabilte__Group__1__Impl rule__Probabilte__Group__2
            {
            pushFollow(FOLLOW_rule__Probabilte__Group__1__Impl_in_rule__Probabilte__Group__12618);
            rule__Probabilte__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Probabilte__Group__2_in_rule__Probabilte__Group__12621);
            rule__Probabilte__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Probabilte__Group__1"


    // $ANTLR start "rule__Probabilte__Group__1__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1342:1: rule__Probabilte__Group__1__Impl : ( ( rule__Probabilte__ProbaAssignment_1 ) ) ;
    public final void rule__Probabilte__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1346:1: ( ( ( rule__Probabilte__ProbaAssignment_1 ) ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1347:1: ( ( rule__Probabilte__ProbaAssignment_1 ) )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1347:1: ( ( rule__Probabilte__ProbaAssignment_1 ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1348:1: ( rule__Probabilte__ProbaAssignment_1 )
            {
             before(grammarAccess.getProbabilteAccess().getProbaAssignment_1()); 
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1349:1: ( rule__Probabilte__ProbaAssignment_1 )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1349:2: rule__Probabilte__ProbaAssignment_1
            {
            pushFollow(FOLLOW_rule__Probabilte__ProbaAssignment_1_in_rule__Probabilte__Group__1__Impl2648);
            rule__Probabilte__ProbaAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getProbabilteAccess().getProbaAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Probabilte__Group__1__Impl"


    // $ANTLR start "rule__Probabilte__Group__2"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1359:1: rule__Probabilte__Group__2 : rule__Probabilte__Group__2__Impl ;
    public final void rule__Probabilte__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1363:1: ( rule__Probabilte__Group__2__Impl )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1364:2: rule__Probabilte__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Probabilte__Group__2__Impl_in_rule__Probabilte__Group__22678);
            rule__Probabilte__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Probabilte__Group__2"


    // $ANTLR start "rule__Probabilte__Group__2__Impl"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1370:1: rule__Probabilte__Group__2__Impl : ( '%' ) ;
    public final void rule__Probabilte__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1374:1: ( ( '%' ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1375:1: ( '%' )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1375:1: ( '%' )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1376:1: '%'
            {
             before(grammarAccess.getProbabilteAccess().getPercentSignKeyword_2()); 
            match(input,22,FOLLOW_22_in_rule__Probabilte__Group__2__Impl2706); 
             after(grammarAccess.getProbabilteAccess().getPercentSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Probabilte__Group__2__Impl"


    // $ANTLR start "rule__VideoGen__VideoAssignment_3"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1396:1: rule__VideoGen__VideoAssignment_3 : ( ruleVideo ) ;
    public final void rule__VideoGen__VideoAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1400:1: ( ( ruleVideo ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1401:1: ( ruleVideo )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1401:1: ( ruleVideo )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1402:1: ruleVideo
            {
             before(grammarAccess.getVideoGenAccess().getVideoVideoParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleVideo_in_rule__VideoGen__VideoAssignment_32748);
            ruleVideo();

            state._fsp--;

             after(grammarAccess.getVideoGenAccess().getVideoVideoParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoGen__VideoAssignment_3"


    // $ANTLR start "rule__Mandatory__VideoSeqAssignment_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1411:1: rule__Mandatory__VideoSeqAssignment_1 : ( ruleVideoSeq ) ;
    public final void rule__Mandatory__VideoSeqAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1415:1: ( ( ruleVideoSeq ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1416:1: ( ruleVideoSeq )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1416:1: ( ruleVideoSeq )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1417:1: ruleVideoSeq
            {
             before(grammarAccess.getMandatoryAccess().getVideoSeqVideoSeqParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleVideoSeq_in_rule__Mandatory__VideoSeqAssignment_12779);
            ruleVideoSeq();

            state._fsp--;

             after(grammarAccess.getMandatoryAccess().getVideoSeqVideoSeqParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Mandatory__VideoSeqAssignment_1"


    // $ANTLR start "rule__Optional__VideoSeqAssignment_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1426:1: rule__Optional__VideoSeqAssignment_1 : ( ruleVideoSeq ) ;
    public final void rule__Optional__VideoSeqAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1430:1: ( ( ruleVideoSeq ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1431:1: ( ruleVideoSeq )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1431:1: ( ruleVideoSeq )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1432:1: ruleVideoSeq
            {
             before(grammarAccess.getOptionalAccess().getVideoSeqVideoSeqParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleVideoSeq_in_rule__Optional__VideoSeqAssignment_12810);
            ruleVideoSeq();

            state._fsp--;

             after(grammarAccess.getOptionalAccess().getVideoSeqVideoSeqParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Optional__VideoSeqAssignment_1"


    // $ANTLR start "rule__Alternative__NameAssignment_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1441:1: rule__Alternative__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Alternative__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1445:1: ( ( RULE_ID ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1446:1: ( RULE_ID )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1446:1: ( RULE_ID )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1447:1: RULE_ID
            {
             before(grammarAccess.getAlternativeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Alternative__NameAssignment_12841); 
             after(grammarAccess.getAlternativeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__NameAssignment_1"


    // $ANTLR start "rule__Alternative__VideoSeqAssignment_3"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1456:1: rule__Alternative__VideoSeqAssignment_3 : ( ruleVideoSeq ) ;
    public final void rule__Alternative__VideoSeqAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1460:1: ( ( ruleVideoSeq ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1461:1: ( ruleVideoSeq )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1461:1: ( ruleVideoSeq )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1462:1: ruleVideoSeq
            {
             before(grammarAccess.getAlternativeAccess().getVideoSeqVideoSeqParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleVideoSeq_in_rule__Alternative__VideoSeqAssignment_32872);
            ruleVideoSeq();

            state._fsp--;

             after(grammarAccess.getAlternativeAccess().getVideoSeqVideoSeqParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alternative__VideoSeqAssignment_3"


    // $ANTLR start "rule__VideoSeq__NameAssignment_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1471:1: rule__VideoSeq__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__VideoSeq__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1475:1: ( ( RULE_ID ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1476:1: ( RULE_ID )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1476:1: ( RULE_ID )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1477:1: RULE_ID
            {
             before(grammarAccess.getVideoSeqAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__VideoSeq__NameAssignment_12903); 
             after(grammarAccess.getVideoSeqAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__NameAssignment_1"


    // $ANTLR start "rule__VideoSeq__TextAssignment_2"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1486:1: rule__VideoSeq__TextAssignment_2 : ( RULE_STRING ) ;
    public final void rule__VideoSeq__TextAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1490:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1491:1: ( RULE_STRING )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1491:1: ( RULE_STRING )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1492:1: RULE_STRING
            {
             before(grammarAccess.getVideoSeqAccess().getTextSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__VideoSeq__TextAssignment_22934); 
             after(grammarAccess.getVideoSeqAccess().getTextSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__TextAssignment_2"


    // $ANTLR start "rule__VideoSeq__DescriptionAssignment_3_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1501:1: rule__VideoSeq__DescriptionAssignment_3_1 : ( ruleDescription ) ;
    public final void rule__VideoSeq__DescriptionAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1505:1: ( ( ruleDescription ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1506:1: ( ruleDescription )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1506:1: ( ruleDescription )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1507:1: ruleDescription
            {
             before(grammarAccess.getVideoSeqAccess().getDescriptionDescriptionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_ruleDescription_in_rule__VideoSeq__DescriptionAssignment_3_12965);
            ruleDescription();

            state._fsp--;

             after(grammarAccess.getVideoSeqAccess().getDescriptionDescriptionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__DescriptionAssignment_3_1"


    // $ANTLR start "rule__VideoSeq__DureeAssignment_4_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1516:1: rule__VideoSeq__DureeAssignment_4_1 : ( ruleDuree ) ;
    public final void rule__VideoSeq__DureeAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1520:1: ( ( ruleDuree ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1521:1: ( ruleDuree )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1521:1: ( ruleDuree )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1522:1: ruleDuree
            {
             before(grammarAccess.getVideoSeqAccess().getDureeDureeParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_ruleDuree_in_rule__VideoSeq__DureeAssignment_4_12996);
            ruleDuree();

            state._fsp--;

             after(grammarAccess.getVideoSeqAccess().getDureeDureeParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__DureeAssignment_4_1"


    // $ANTLR start "rule__VideoSeq__ProbabilteAssignment_5_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1531:1: rule__VideoSeq__ProbabilteAssignment_5_1 : ( ruleProbabilte ) ;
    public final void rule__VideoSeq__ProbabilteAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1535:1: ( ( ruleProbabilte ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1536:1: ( ruleProbabilte )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1536:1: ( ruleProbabilte )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1537:1: ruleProbabilte
            {
             before(grammarAccess.getVideoSeqAccess().getProbabilteProbabilteParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_ruleProbabilte_in_rule__VideoSeq__ProbabilteAssignment_5_13027);
            ruleProbabilte();

            state._fsp--;

             after(grammarAccess.getVideoSeqAccess().getProbabilteProbabilteParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VideoSeq__ProbabilteAssignment_5_1"


    // $ANTLR start "rule__Description__TextAssignment_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1546:1: rule__Description__TextAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Description__TextAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1550:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1551:1: ( RULE_STRING )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1551:1: ( RULE_STRING )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1552:1: RULE_STRING
            {
             before(grammarAccess.getDescriptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Description__TextAssignment_13058); 
             after(grammarAccess.getDescriptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Description__TextAssignment_1"


    // $ANTLR start "rule__Duree__TempsAssignment_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1561:1: rule__Duree__TempsAssignment_1 : ( RULE_INT ) ;
    public final void rule__Duree__TempsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1565:1: ( ( RULE_INT ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1566:1: ( RULE_INT )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1566:1: ( RULE_INT )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1567:1: RULE_INT
            {
             before(grammarAccess.getDureeAccess().getTempsINTTerminalRuleCall_1_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Duree__TempsAssignment_13089); 
             after(grammarAccess.getDureeAccess().getTempsINTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Duree__TempsAssignment_1"


    // $ANTLR start "rule__Probabilte__ProbaAssignment_1"
    // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1576:1: rule__Probabilte__ProbaAssignment_1 : ( RULE_INT ) ;
    public final void rule__Probabilte__ProbaAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1580:1: ( ( RULE_INT ) )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1581:1: ( RULE_INT )
            {
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1581:1: ( RULE_INT )
            // ../org.xtext.example.videoGen.ui/src-gen/org/xtext/example/videoGen/ui/contentassist/antlr/internal/InternalVideoGen.g:1582:1: RULE_INT
            {
             before(grammarAccess.getProbabilteAccess().getProbaINTTerminalRuleCall_1_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Probabilte__ProbaAssignment_13120); 
             after(grammarAccess.getProbabilteAccess().getProbaINTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Probabilte__ProbaAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleVideoGen_in_entryRuleVideoGen61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoGen68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__0_in_ruleVideoGen94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideo_in_entryRuleVideo121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideo128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Video__Alternatives_in_ruleVideo154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_entryRuleMandatory181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMandatory188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__0_in_ruleMandatory214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_entryRuleOptional241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOptional248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__Group__0_in_ruleOptional274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternative_in_entryRuleAlternative301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAlternative308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternative__Group__0_in_ruleAlternative334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_entryRuleVideoSeq361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVideoSeq368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__0_in_ruleVideoSeq394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDescription_in_entryRuleDescription421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDescription428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Description__Group__0_in_ruleDescription454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDuree_in_entryRuleDuree481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDuree488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Duree__Group__0_in_ruleDuree514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProbabilte_in_entryRuleProbabilte541 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProbabilte548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Probabilte__Group__0_in_ruleProbabilte574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMandatory_in_rule__Video__Alternatives610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptional_in_rule__Video__Alternatives627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAlternative_in_rule__Video__Alternatives644 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__0__Impl_in_rule__VideoGen__Group__0674 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__1_in_rule__VideoGen__Group__0677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__1__Impl_in_rule__VideoGen__Group__1735 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__2_in_rule__VideoGen__Group__1738 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__VideoGen__Group__1__Impl766 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__2__Impl_in_rule__VideoGen__Group__2797 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__3_in_rule__VideoGen__Group__2800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__VideoGen__Group__2__Impl828 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__3__Impl_in_rule__VideoGen__Group__3859 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__4_in_rule__VideoGen__Group__3862 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoGen__VideoAssignment_3_in_rule__VideoGen__Group__3__Impl889 = new BitSet(new long[]{0x000000000001C002L});
    public static final BitSet FOLLOW_rule__VideoGen__Group__4__Impl_in_rule__VideoGen__Group__4920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__VideoGen__Group__4__Impl948 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__0__Impl_in_rule__Mandatory__Group__0989 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__1_in_rule__Mandatory__Group__0992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Mandatory__Group__0__Impl1020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__Group__1__Impl_in_rule__Mandatory__Group__11051 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Mandatory__VideoSeqAssignment_1_in_rule__Mandatory__Group__1__Impl1078 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__Group__0__Impl_in_rule__Optional__Group__01112 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Optional__Group__1_in_rule__Optional__Group__01115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Optional__Group__0__Impl1143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__Group__1__Impl_in_rule__Optional__Group__11174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Optional__VideoSeqAssignment_1_in_rule__Optional__Group__1__Impl1201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternative__Group__0__Impl_in_rule__Alternative__Group__01235 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_rule__Alternative__Group__1_in_rule__Alternative__Group__01238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Alternative__Group__0__Impl1266 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternative__Group__1__Impl_in_rule__Alternative__Group__11297 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_rule__Alternative__Group__2_in_rule__Alternative__Group__11300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternative__NameAssignment_1_in_rule__Alternative__Group__1__Impl1327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternative__Group__2__Impl_in_rule__Alternative__Group__21358 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Alternative__Group__3_in_rule__Alternative__Group__21361 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Alternative__Group__2__Impl1389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternative__Group__3__Impl_in_rule__Alternative__Group__31420 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Alternative__Group__4_in_rule__Alternative__Group__31423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alternative__VideoSeqAssignment_3_in_rule__Alternative__Group__3__Impl1452 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_rule__Alternative__VideoSeqAssignment_3_in_rule__Alternative__Group__3__Impl1464 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_rule__Alternative__Group__4__Impl_in_rule__Alternative__Group__41497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Alternative__Group__4__Impl1525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__0__Impl_in_rule__VideoSeq__Group__01566 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__1_in_rule__VideoSeq__Group__01569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__VideoSeq__Group__0__Impl1597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__1__Impl_in_rule__VideoSeq__Group__11628 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__2_in_rule__VideoSeq__Group__11631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__NameAssignment_1_in_rule__VideoSeq__Group__1__Impl1658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__2__Impl_in_rule__VideoSeq__Group__21689 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__3_in_rule__VideoSeq__Group__21692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__TextAssignment_2_in_rule__VideoSeq__Group__2__Impl1719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__3__Impl_in_rule__VideoSeq__Group__31749 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__4_in_rule__VideoSeq__Group__31752 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_3__0_in_rule__VideoSeq__Group__3__Impl1779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__4__Impl_in_rule__VideoSeq__Group__41810 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__5_in_rule__VideoSeq__Group__41813 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_4__0_in_rule__VideoSeq__Group__4__Impl1840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group__5__Impl_in_rule__VideoSeq__Group__51871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_5__0_in_rule__VideoSeq__Group__5__Impl1898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_3__0__Impl_in_rule__VideoSeq__Group_3__01941 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_3__1_in_rule__VideoSeq__Group_3__01944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__VideoSeq__Group_3__0__Impl1972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_3__1__Impl_in_rule__VideoSeq__Group_3__12003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__DescriptionAssignment_3_1_in_rule__VideoSeq__Group_3__1__Impl2030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_4__0__Impl_in_rule__VideoSeq__Group_4__02064 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_4__1_in_rule__VideoSeq__Group_4__02067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__VideoSeq__Group_4__0__Impl2095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_4__1__Impl_in_rule__VideoSeq__Group_4__12126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__DureeAssignment_4_1_in_rule__VideoSeq__Group_4__1__Impl2153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_5__0__Impl_in_rule__VideoSeq__Group_5__02187 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_5__1_in_rule__VideoSeq__Group_5__02190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__VideoSeq__Group_5__0__Impl2218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__Group_5__1__Impl_in_rule__VideoSeq__Group_5__12249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VideoSeq__ProbabilteAssignment_5_1_in_rule__VideoSeq__Group_5__1__Impl2276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Description__Group__0__Impl_in_rule__Description__Group__02310 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Description__Group__1_in_rule__Description__Group__02313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Description__Group__0__Impl2341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Description__Group__1__Impl_in_rule__Description__Group__12372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Description__TextAssignment_1_in_rule__Description__Group__1__Impl2399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Duree__Group__0__Impl_in_rule__Duree__Group__02433 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Duree__Group__1_in_rule__Duree__Group__02436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Duree__Group__0__Impl2464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Duree__Group__1__Impl_in_rule__Duree__Group__12495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Duree__TempsAssignment_1_in_rule__Duree__Group__1__Impl2522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Probabilte__Group__0__Impl_in_rule__Probabilte__Group__02556 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Probabilte__Group__1_in_rule__Probabilte__Group__02559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Probabilte__Group__0__Impl2587 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Probabilte__Group__1__Impl_in_rule__Probabilte__Group__12618 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Probabilte__Group__2_in_rule__Probabilte__Group__12621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Probabilte__ProbaAssignment_1_in_rule__Probabilte__Group__1__Impl2648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Probabilte__Group__2__Impl_in_rule__Probabilte__Group__22678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Probabilte__Group__2__Impl2706 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideo_in_rule__VideoGen__VideoAssignment_32748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_rule__Mandatory__VideoSeqAssignment_12779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_rule__Optional__VideoSeqAssignment_12810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Alternative__NameAssignment_12841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVideoSeq_in_rule__Alternative__VideoSeqAssignment_32872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__VideoSeq__NameAssignment_12903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__VideoSeq__TextAssignment_22934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDescription_in_rule__VideoSeq__DescriptionAssignment_3_12965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDuree_in_rule__VideoSeq__DureeAssignment_4_12996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProbabilte_in_rule__VideoSeq__ProbabilteAssignment_5_13027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Description__TextAssignment_13058 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Duree__TempsAssignment_13089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Probabilte__ProbaAssignment_13120 = new BitSet(new long[]{0x0000000000000002L});

}