/*
 * generated by Xtext
 */
package org.xtext.example.videoGen;

import org.eclipse.xtext.junit4.IInjectorProvider;

import com.google.inject.Injector;

public class VideoGenUiInjectorProvider implements IInjectorProvider {
	
	@Override
	public Injector getInjector() {
		return org.xtext.example.videoGen.ui.internal.VideoGenActivator.getInstance().getInjector("org.xtext.example.videoGen.VideoGen");
	}
	
}
